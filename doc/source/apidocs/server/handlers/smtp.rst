.. _SMTPHandler:

:mod:`b.s.h.smtp` -- SMTP handlers
==================================

.. autoclass:: breadcrumb.server.handlers.smtp.SMTPHandler
    :members: newpoint
