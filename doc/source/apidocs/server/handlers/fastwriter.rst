.. _FastKMLHandler:

:mod:`b.s.h.fastwriter` -- Fast flat file writers (KML, GPX)
============================================================

.. autoclass:: breadcrumb.server.handlers.fastwriter.FastFileWriter
    :members:
