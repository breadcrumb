
:mod:`b.s.h.common` -- Common handler tools and mixins
======================================================

.. _PointQueueHandler:

:mod:`b.s.h.c.PointQueueHandler` -- a handler with a queue of points
--------------------------------------------------------------------

.. autoclass:: breadcrumb.server.handlers.common.PointQueueHandler
    :members:

:mod:`b.s.h.c.Renderer` -- rendering sequences of points to strings
-------------------------------------------------------------------

.. autoclass:: breadcrumb.server.handlers.common.Renderer
    :members:
