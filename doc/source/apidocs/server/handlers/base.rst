.. _basehandlers:

:mod:`b.s.h.base` -- Basic handlers
===================================

.. _BaseHandler:

:mod:`b.s.h.b.BaseHandler` -- Base handler (superclass)
-------------------------------------------------------

.. autoclass:: breadcrumb.server.handlers.base.BaseHandler
    :members:

.. _SequentialHandler:

:mod:`b.s.h.b.SequentialHandler` -- using several handlers
----------------------------------------------------------

.. autoclass:: breadcrumb.server.handlers.base.SequentialHandler
    :members:

.. _EchoHandler:

:mod:`b.s.h.b.EchoHandler` -- Echoing output
--------------------------------------------

.. autoclass:: breadcrumb.server.handlers.base.EchoHandler
    :members:
