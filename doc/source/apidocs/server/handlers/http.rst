.. _HTTPHandler:

:mod:`b.s.h.http` -- HTTP handlers
==================================

.. autoclass:: breadcrumb.server.handlers.http.HTTPHandler

