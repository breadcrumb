.. _TkGUIHandler:

:mod:`b.s.h.tkgui` -- Tk server GUI
===================================

.. autoclass:: breadcrumb.server.handlers.tkgui.TkGUIHandler
