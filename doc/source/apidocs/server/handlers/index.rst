.. _handlers:

-------------------------------
:mod:`b.s.handlers` -- handlers
-------------------------------

.. toctree::
    :maxdepth: 2

    base
    common
    fastwriter
    http
    smtp
    tkgui

.. automodule:: breadcrumb.server.handlers
