.. _server:

======
Server
======

Servers accept packets from :ref:`clients<client>`, encoded using the Breadcrumb :ref:`protocol`. They decode them, and pass the contents down to :ref:`handlers`, which do all of the heavy lifting.

.. toctree::
    :maxdepth: 2

    handlers/index.rst

---------------
:mod:`b.server`
---------------

.. automodule:: breadcrumb.server

--------------
:mod:`b.s.net`
--------------

.. automodule:: breadcrumb.server.net
    :members:

-------------------
:mod:`b.s.decoding`
-------------------

.. automodule:: breadcrumb.server.decoding
    :members:
