.. _tools:

=====
Tools
=====

--------------------
:mod:`b.t.faketrack`
--------------------

.. automodule:: breadcrumb.tools.faketrack
    :members: nextdirection, nexthoplength, interpolate, nmea, newrandomunitvec, newrandom180unitvec
