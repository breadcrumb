.. _nmea:

===============================
:mod:`b.c.nmea` -- NMEA parsing
===============================

This module is responsible for parsing NMEA sentences. It returns points.


