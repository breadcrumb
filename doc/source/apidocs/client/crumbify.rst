.. _crumbify:

====================================
:mod:`b.c.crumbify` -- point packing
====================================

.. automodule:: breadcrumb.client.crumbify
    :members:
