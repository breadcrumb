.. _deltas:

============================================
:mod:`b.c.deltas` -- Difference calculations
============================================

.. _point:

This module defines the class that describes data points, which is used throughout the rest of the client.

This class implements difference checking with the the ``<`` operator -- ``oldpoint < newpoint`` will automatically do the right thing (in this case, return True if the new point is significantly different from the old point).

What 'significantly' means is defined by the current set of thresholds. 
