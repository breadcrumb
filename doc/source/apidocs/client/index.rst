.. _client:

======
Client
======

Clients read data from GPS devices (real serial ports with live data, gpsd interfaces, or replayed sentences), decide if the new data is sufficiently different from the last important point, and then send an encoded version ('crumb') to the server.

.. toctree::
    :maxdepth: 2

    gps
    nmea
    deltas
    crumbify

