.. _gps:

================================
:mod:`b.c.gps` -- GPS interfaces
================================

The ``b.c.gps`` module provides several GPS classes, which are all similarly-behaving interfaces for several kinds of GPSes.

----------------------------------------------
``connect()`` -- automagical GPS configuration
----------------------------------------------

.. autofunction:: breadcrumb.client.gps.connect

--------------------
:mod:`b.c.g.nmeaGPS`
--------------------

This is a GPS interface for any source of NMEA sentences. It iterates over a given iterable (this is typically either a serial GPS or a list of replayed sentences) and yields points in the process.

This depends on the :ref:`NMEA parsing module<nmea>` to do the actual heavy lifting.

.. autoclass:: breadcrumb.client.gps.nmeaGPS

--------------------
:mod:`b.c.g.gpsdGPS`
--------------------

.. autoclass:: breadcrumb.client.gps.gpsdGPS
