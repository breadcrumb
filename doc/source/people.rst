======
People 
======

:Author: Laurens Van Houtven
:Date: |today|
:License: GNU Free Documentation License 1.3 or later
:Copyright: Copyright (C) 2009 Laurens Van Houtven

.. contents::

Authors
=======

Laurens Van Houtven (lvh)
-------------------------
:Real name: Laurens Van Houtven
:Email: lvh _at_ laurensvh.be
:Role: Founder and lead dev

Contributors
============

#python
-------

The following people in #python (on Freenode_) have been a huge help during the development in this project:
    - habnabit (without whom I would never have learned any Python)
    - arkanes
    - verte

Thanks a lot! *-- lvh*

.. _Freenode: http://freenode.net/
