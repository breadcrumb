.. _handlermessages:

================
Handler messages
================

Handler messages are messages encoded in crumbs, used by the client to send specific messages to one or more handlers.

-----------
Identifiers
-----------

A handler identifier is a single unsigned byte (``'B'`` in Python's ``struct`` module). Handler identifiers are the most important method for handlers to decide if they want to do something with a message or not.

The server always passes the message (calls the ``newmessage(id, msg)`` method) -- it's up to the handler to decide if it does something with it or not.

The orthodox way to do this is with ``self.identifiers``, defined in the base handler, which is a set. By default, it contains the wildcard handler identifier. Handlers should add their own matching identifiers to this set. As long as that is the only check a handler wants to do, deciding if you want to use a message or not becomes a trivial set membership test::

    if not id in self.identifiers:
        return

In the rest of this document, ``X`` (uppercase letter X) can be any hexadecimal digit, unless that digit has been explicitly assigned to something else.

+------------+--------------------------------------------------------+
| Byte       | Handler t                                              |
+============+========================================================+
| ``'\x00'`` | Wildcard (:ref:`Base handler<BaseHandler>`)            |
+------------+--------------------------------------------------------+
| ``'\x01'`` | :ref:`Sequential handler<SequentialHandler>`           | 
+------------+--------------------------------------------------------+
| ``'\x02'`` | :ref:`Echo handler<EchoHandler>`                       |
+------------+--------------------------------------------------------+
| ``'\x03'`` | :ref:`Point queue handler<PointQueueHandler>`          |
+------------+--------------------------------------------------------+
| ``'\x0X'`` | Unused (should not be used by end users)               |
+------------+--------------------------------------------------------+
| ``'\x10'`` | Matches any database handler                           |
+------------+--------------------------------------------------------+
| ``'\x11'`` | The SQLAlchemy-based database handler                  |
+------------+--------------------------------------------------------+
| ``'\x12'`` | The SQLite-base database handler                       |
+------------+--------------------------------------------------------+
| ``'\x20'`` | Matches any file writer                                |
+------------+--------------------------------------------------------+
| ``'\x21'`` | Matches the :ref:`FastKML handler<FastKMLHandler>`     |
+------------+--------------------------------------------------------+
| ``'\x30'`` | Matches any HTTP, SMTP, IRC or IM format.              |
+------------+--------------------------------------------------------+
| ``'\x31'`` | The :ref:`HTTP handler<HTTPHandler>`.                  |
+------------+--------------------------------------------------------+
| ``'\x32'`` | The :ref:`SMTP handler<SMTPHandler>`.                  |
+------------+--------------------------------------------------------+
| ``'\xdX'`` | Reserved for end-users                                 |
+------------+--------------------------------------------------------+
| ``'\xeX'`` | Reserved for end-users                                 |
+------------+--------------------------------------------------------+
| ``'\xfX'`` | Reserved for end-users                                 |
+------------+--------------------------------------------------------+

In case the link between the handlers in ``\x3X`` wasn't obvious, they all produce output that ends up being human-readable. 

Please note that even though ``'\x00'`` is a wildcard, there is no guarantee that it will match a handler. If the handler overwrites the ``newmessage`` method of its superclass (:ref:`BaseHandler`), the check can be circumvented. Again, this is the responsibility of the respective handlers.

Note that using a handler identifier specifically reserved for end-users is the only way to target a specific handler *instance*. This is done because direct identifier to instance mapping is very hard, and realizing it would result in very poorly designed code, where (for example) the client knows about hash() values of server objects. The implications of that (what happens when the server has a brownout?) are left as a nightmare to the reader.
