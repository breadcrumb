===================
Release engineering
===================

:Author: Laurens Van Houtven
:Date: |today|
:License: GNU Free Documentation License 1.3 or later
:Copyright: Copyright (C) 2009 Laurens Van Houtven

.. contents::

Versioning
==========
These releases are scheduled to appear regularly:
    - stable releases
    - maintenance releases
    - nightlies 

Stable releases
---------------
Stable releases are released when they're done.

The version of stable releases should look like this: M.m.0, with M the major revision and m the minor revision.

Stable releases should be made in their own branch, which is called release-M.m, and tagged release-M.m.0.

**Example** The second minor revision of the first major revision of stable releases is called '1.2.0' and is tagged 'release-1.2.0'. The branch is called 'release-1.2'.

Maintenance releases
--------------------
Maintenance releases are a set of bugfixes for a stable release. They don't contain any new features. This is either a condensed set of smaller bugfixes (typos, uncommon fringe cases,...) or a serious bugfix. 

The version of maintenance releases should look like this: M.m.r, with M the major revision, m the minor revision, and r the maintenance release number.

These releases should happen in the branch of their parent (stable) release.

**Example** The first minor revision of the example above would have version number '1.2.1'. (The 'zeroth' minor revision is the stable release.) It is tagged 'release-1.2.1', and it lives in repository branch 'release-1.2'.

Nightlies
---------

Nightlies should have a version of the date on which they were built, in ddmmyyyy format. For example, a nightly built on the first of March 2009 would have ``01032009`` as a version number.
