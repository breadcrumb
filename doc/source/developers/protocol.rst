.. _protocol:

========
Protocol
========

:Author: Laurens Van Houtven
:Date: |today| 
:License: GNU Free Documentation License 1.3 or later
:Copyright: Copyright (C) 2009 Laurens Van Houtven

.. contents::

This document describes the binary protocol used for communication between Breadcrumb clients (typically mobile devices) and servers. It describes the length and content of messages.

Mandatory fields
----------------

Header
~~~~~~
:data type: unsigned char (``'B'`` in the ``struct`` module)
:length: 8 bits (1 byte)
:position: first byte of any crumb

This field signals what other (optional) fields are in the crumb.

The byte should be interpreted as a sequence of 8 bits, each representing a boolean value (1 is True, 0 is False).

The meaning of the bits is (in order):
    * Presence of a timestamp
    * Presence of coordinates (latitude and longitude)
    * Presence of a velocity
    * Presence of a true heading
    * Presence of a handler message (implies presence of a handler identifier)
    * Unused (should be False)
    * Unused (should be False)
    * Unused (should be False)

This byte is stored in little endian.

**Example** A message contains a timestamp and a handler message. It would have the following sequence of boolean flags::

    [True, False, False, False, True, False, False, False]
       \_ Timestamp               \_ Handler message

This value is 17 (0x11). The byte that is sent has this as its ordinal value, or, in Python notation, ``\x11``.

Checksum
~~~~~~~~
:data type: unsigned char (``'B'`` in the ``struct`` module)
:length: 8 bits (1 byte)
:position: last byte of any crumb

The checksum is created by taking the a sequence of one zero followed by the ordinal values of the bytes in the crumb (without the checksum byte, of course) and XOR'ing them, left to right.

Optional fields
---------------
Optional fields are described here in the order in which they appear in a crumb, if they are present. For example, if both coordinates and a timestamp are present, the order in which they appear in the crumb **must** be timestamp, latitude, longitude.

Timestamp
~~~~~~~~~
:data type: unsigned long integer (``'L'`` in the ``struct`` module)
:length: 32 bits (4 bytes)

Contains a number of integer seconds since the UNIX epoch (midnight, 1 Jan 1970).

Unlike real UNIX (POSIX) timestamps, this doesn't suffer from the `year 2038 problem`_, because it's unsigned. Obviously, it will run into a similar problem, just twice as far away from the epoch as POSIX systems will. By that time, this software is extremely likely to be obsolete. If not, we can always turn this into a 64 bit uint, delaying the problem by another 290 billion years.

.. _year 2038 problem: http://en.wikipedia.org/wiki/Year_2038_Problem

Latitude
~~~~~~~~
:data type: single precision float (``'f'`` in the ``struct`` module)
:length: 32 bits (4 bytes)

Stored in `decimal degrees`_. From the protocol's point of view, this field is identical to a longitude.

.. _decimal degrees: http://en.wikipedia.org/wiki/Decimal_degrees

Longitude
~~~~~~~~~
:data type: single precision float (``'f'`` in the ``struct`` module)
:length: 32 bits (4 bytes)

Stored in `decimal degrees`_. From the protocol's point of view, this field is identical to a latitude.

Velocity
~~~~~~~~
:data type: single precision float (``'f'`` in the ``struct`` module)
:length: 32 bits (4 bytes)

Stored in meters per second.

True heading
~~~~~~~~~~~~
:data type: single precision float (``'f'`` in the ``struct`` module)
:length: 32 bits (4 bytes)

Stored in degrees.

The protocol currently has no support for magnetic headings.

Handler identifier
~~~~~~~~~~~~~~~~~~
:data type: char (``'c'`` in the ``struct`` module)
:length: 1 byte 

Identifies the handler to which the handler message (which always follows this field) is to be sent. This can be a wildcard, an identifier that matches an entire category of handlers, or one handler specifically.

For more information, see the handler documentation.

Handler message
~~~~~~~~~~~~~~~
:data type: string, char[] (``'8s'`` in the struct module)
:length: 8 bytes


