=========
Licensing
=========

:Author: Laurens Van Houtven
:Date: |today|
:License: GNU Free Documentation License 1.3 or later
:Copyright: Copyright (C) 2009 Laurens Van Houtven

The material on this web site is released under the GFDL version 1.3 or later, which is distributed together with this package under LICENSE_. Most of the code documented by this material is released under the GNU GPLv3 or later. That license is distributed with the code (see a file called ``LICENSE`` in the root directory or the ``breadcrumb/`` directory).

.. _LICENSE: ./_static/LICENSE
