.. _config:

===================
Configuration files
===================

:Author: Laurens Van Houtven
:Date: |today|
:License: GNU Free Documentation License 1.3 or later
:Copyright: Copyright (C) 2009 Laurens Van Houtven

.. contents::

------------
Introduction
------------

The syntax for config files is INI-like. This is done because the parser for it comes with nearly all Python installations, so it's much more portable than, say, YAML (the syntax of which we prefer over INI). 

Priorities
==========

Several configuration options can be specified at more than one level. The general rule is: more specific means higher priority. If two things are equally specific, the one that is applied later takes precedence. (This sounds more complicated than it is. It's the only way that makes real sense.)

The priorities are (from high to low):

1. Settings changed in a running instance
2. Command line settings
3. Configuration file settings (you are here)
4. Default values (in the ``consts`` module)

--------------------
Server configuration
--------------------

Server section
==============

The title of this section is ``[server]``.

+-----------+------------------------------------------------------+
| Key       | Meaning                                              |
+-----------+------------------------------------------------------+
| ``port``  | The port to connect to.                              |
+-----------+------------------------------------------------------+
| ``encap`` | The encapsultation used for the Breadcrumb protocol. |
+-----------+------------------------------------------------------+

Handler sections
================

These sections describe handlers used by the server. The title of this section can be any valid INI section title, except ``[server]`` (obviously, since that's used for general server configuration).

The title is used as a description for the handler it describes.

The server starting script that uses this file automatically creates a :ref:`sequential handler<SequentialHandler>`. You do not need to add one yourself.

+-----------+-----------------------------------------------------------------------+
| Key       | Meaning                                                               |
+-----------+-----------------------------------------------------------------------+
| ``class`` | The name of the class, including the module hierarchy.                |
+-----------+-----------------------------------------------------------------------+
| ``path``  | Path to the module from which the handler can be imported. (optional) | 
+-----------+-----------------------------------------------------------------------+

Any additional key/value pairs are passed to the handler constructor as kwargs.
