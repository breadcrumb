============
Introduction
============

:Author: Laurens Van Houtven
:Date: |today|
:License: GNU Free Documentation License 1.3 or later
:Copyright: Copyright (C) 2009 Laurens Van Houtven

.. contents::

What is Breadcrumb?
===================

Commonly used terms
===================
If you want to understand anything in the documentation, you should probably get familiar with the terminology.

A device (usually mobile) runs a program called the **client**. The client reads data from a GPS device (directly or indirectly). 

At some point in time, the client decides to send that data. The data is encoded as a sequence of bytes. This sequence of bytes is called a **crumb**.

The software that accepts and decodes the crumb is called the **server**. The server passes the decoded data to a **handler**, which does some specific action, such as printing to to a terminal, sending email, or updating a database.

Software that does further processing after the handler is called a **backend**. This might be something that displays points on a map, or does more advanced statistical analysis, or anything else you can think of.

Why we love Breadcrumb
======================

There's a :ref:`propaganda page <propaganda>` that tries to explain why we love Breadcrumb (and hopefully, you will too).
