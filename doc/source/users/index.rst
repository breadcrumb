============
User's guide
============

Contents:

.. toctree::
    :maxdepth: 2

    intro
    propaganda
    config

This is the index page for the bits of documentation that are specifically targetted at users.

