.. _propaganda:

==========
Propaganda
==========

:Author: Laurens Van Houtven
:Date: |today|
:License: GNU Free Documentation License 1.3 or later
:Copyright: Copyright (C) 2009 Laurens Van Houtven

.. contents::

*Disclaimer*: This page is written and maintained by the main developer. It's hardly neutral, but it is mostly based on fact.

In short
========

    - runs almost everywhere
    - free as in beer and free as in speech
    - efficient protocol
    - easy to extend and develop for
    - best practices, as often as possible (docs, tests...)

Portable
========
Breadcrumb's client software runs on pretty much anything that runs Python and has a serial port for GPS access.

Odds are your platform is supported. We support serial ports (including Bluetooth ports) on the following platforms:

    - Linux, BSD, and most other POSIXy systems
    - Windows CE (aka Windows Mobile)
    - Windows 98, ME, 2000, XP, Vista
    - Anything that runs Java (Jython)
    - Anything that runs .NET (IronPython)

There is also support planned for gpsd_, so you won't even need a working serial port (although gpsd might still need a serial port, of course).

.. _gpsd: http://gpsd.berlios.de/

Extensible
==========

If you don't find what you need, it's really easy to extend Breadcrumb.

Powered by Python
-----------------
Breadcrumb is written in Python_, and is designed to be compatible with the 2.x branch of Python version 2.4 or later (this is not a hard guarantee, and not an excuse to keep using ancient versions of Python!).

Python is a really nice language. It's pretty easy to write, while still being easy to read and easy to learn. Most people can read and understand a lot of code with zero Python experience. You can probably write a handler with minimal experience, the tutorial_ and some sample handler code next to you.

Python is an interpreted language, which has sped up our development cycle a lot. Testing and debugging is quick and painless.

.. _Python: http://www.python.org/
.. _tutorial: http://docs.python.org/tutorial/

Modular design
--------------
Most common tasks (sending email, updating a database, reporting to a terminal...) can be done with handlers that come with Breadcrumb. If you don't find what you need, you can always roll your own handler -- it's not as hard as it sounds.

The handlers that ship with Breadcrumb aren't special. They inhert from a class, and implement a few methods. Your handlers can do exactly the same -- they're not second-class citizens.

You could write complicated handlers that raise alarms when the client comes close to your house (so you can hide the evidence of a poker night really quickly), or when two clients come close to each other, for example.

Since they're just Python scripts, you can use them together with any existing Python library you want. (weather forecasts, phase of the moon, whatever) 

Open source
-----------
All of Breadcrumb's source is GPLv3. You're free to do a lot of stuff with it. You can fork, modify, extend... as long as you publish changes under the same license, granting other people the same freedoms you were granted yourself. For more information, see the Licensing_ page.

Because we use git_, developing on your own is trivial. You don't need a 'commit bit' to start hacking.

.. _Licensing: ./license.html
.. _git: http://git.or.cz

Cheap
=====

Free software
-------------
Breadcrumb is not only  *free as in speech*, it's *free as in beer* as well. You don't have to pay anyone anything for the *right* to use it.

As cheap as it gets
-------------------

Unfortunately, we haven't convinced any mobile internet providers to give everyone free mobile internet yet. Mobile internet still costs money, and it can be quite expensive. Thankfully, many providers have prepaid data plans (no subscription costs), which are perfect for use with Breadcrumb, because its traffic is typically low volume but with a long time spent connected.

The protocol used to communicate between client and server is designed to be as compact as possible. A typical crumb, containing a timestamp and location (latitude and longitude) is only *14 bytes*! Here's what's in it:

:header: 1 byte (unsigned char)
:timestamp: 4 bytes (unsigned long integer)
:latitude: 4 bytes (one single precision float)
:longitude: 4 bytes (one single precision float)
:checksum: 1 byte (unsigned char)

As long as you have a data plan (and you pay by volume, not time), you almost can't afford *not* to have your car tracked.

The :ref:`protocol` can do a lot more, including cool stuff like customizable handler messages.

High quality
============

Documentation
-------------

We try to keep this project well-documented as much as possible. This is done with a combination of Sphinx_ docs (you're looking at them) and API documentation generated from docstrings by pydoctor_. You can find links to both on the :ref:`index page<index>`.

.. _pydoctor: http://codespeak.net/~mwh/pydoctor/
.. _Sphinx: http://sphinx.pocoo.org/

Issue tracking
--------------
We use ditz_, a distributed issue tracking system. The issues database is perpetually kept in sync with the git repository, so there's never any question as to which branch fixed what bug. Developers can do their coding, debugging and fixing anywhere -- ditz borrows git's flexibility, and there's no dependency on an internet connection to get work done.

.. _ditz: http://ditz.rubyforge.org/

Software tests
--------------
Every time we fix something, we make a unit test. After all, the last set of unit tests didn't manage to catch the bug. Because tests are so cheap to run, we don't get perpetually recurring bugs.

Running tests is easy, using any test suite you want (nosetests is the most commonly used).
