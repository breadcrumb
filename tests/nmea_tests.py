# -*- coding: utf-8 -*-
""" Tests for nmea.py, the NMEA sentence parsing module. """

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import testing
import unittest
import breadcrumb.client.nmea as nmea

class NMEATest(unittest.TestCase):
    def setUp(self):
        pass

    def test_noninteresting(self):
        """Testing if the filter correctly filters uninteresting sentences."""
        # Filter for non-interesting (usually propietary) sentences:
        sentence = '$XXGGA123456789*12'
        self.assertFalse(nmea.interesting(sentence), "Filter for most \
proprietary ('non-interesting') sentences, non-interesting sentence.")
        self.assertEqual(nmea.parse(sentence), None, "Implementation of the \
interesting filter in the parse() function, non-interesting sentence.")
        
    def test_interesting(self):
        """Testing if the filter correctly detects interesting sentences."""
        sentence = '$GPGGA123456789*12'
        self.assertTrue(nmea.interesting(sentence), "Filter for most \
proprietary ('non-interesting') sentences, interesting sentence.")
        
    def testchecksumimplementation(self):
        """Testing the implementation of the checksum functions."""
        sentence = '$GPGGA123456789*12'
        self.assertEqual(nmea.parse(sentence), None, 'NMEA sentence with bad\
checksum, should be ignored') 

    def testunpack_withcsum(self):
        """Testing the NMEA unpacker on a sentence with a checksum."""
        # Sentence with a checksum
        self.assertEqual(nmea.unpack(
            '$GPGGA,spam,eggs,bacon*00'),
            ['GGA', 'spam', 'eggs', 'bacon'],
            'Unpacking of an NMEA sentence with a checksum'
        )
    def testunpack_nocsum(self):
        """Testing the NMEA unpacker on a sentence without a checksum."""
        self.assertEqual(nmea.unpack(
            '$GPGGA,spam,eggs,bacon*'),
            ['GGA', 'spam', 'eggs', 'bacon'],
            'Unpacking of an NMEA sentence without a checksum'
        )

    def testunpack_nocsum(self):
        """Testing the NMEA unpacker on a sentence with bad checksums."""
        self.assertEqual(nmea.unpack(
            '$GPGGA,spam,eggs,bacon'),
            None,
            "Unpacking of an bad NMEA sentence (no checksum marker)"
        )

    def testgetcoordinates(self):
        """Testing coordinate extraction from their ugly NMEA notation."""
        origdata = {
            'lat_fl': '1000.00', # 10 degrees, 00 minutes, 0 decmins 
            'lat_ns': 'N',
            'lon_fl': '2000.00', # 20 degrees, 00 minutes, 0 decmins
            'lon_ew': 'E'
        }

        data = dict(origdata)
        keys = origdata.keys()
        data = nmea.getcoordinates(data)

        for key in keys:
            self.assertFalse(key in data, 'junk key %s removed' % key)

        for coord in ['latitude', 'longitude']:
            self.assertTrue(coord in data, 'coordinate key %s in dict' % coord)

        self.assertEqual(data['latitude'], 10.0,  'found correct latitude in \
parsed coordinates')
        self.assertEqual(data['longitude'], 20.0, 'found correct longitude in \
parsed coordinates')

        # Try on the other two hemispheres, to check for sign handling.
        data = dict(origdata)
        data['lat_ns'], data['lon_ew'] = 'S', 'W'
        data = nmea.getcoordinates(data)
        self.assertEqual(data['latitude'], -10.0,  'found correct latitude in \
parsed coordinates')
        self.assertEqual(data['longitude'], -20.0, 'found correct longitude in \
parsed coordinates')

if __name__ == "__main__":
    unittest.main()
