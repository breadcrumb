# -*- coding: utf-8 -*-
""" Tests for crumbify.py, the Breadcrumb protocol serializer. """

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import testing
import unittest
import breadcrumb.client.crumbify as crumbify

class CrumbifyTest(unittest.TestCase):
    def setUp(self):
        pass

    def testpackbools_autopadding(self):
        """Testing if automatical False padding when packing booleans works.

        This was a small problem with the original implementation. The new way
        of doing it doesn't actually do any padding, so this shouldn't be a
        problem anymore."""
        
        unpadded = [True]
        padded = unpadded + [False] * (8-len(unpadded)) # 8 bits per byte
        self.assertEqual(
            crumbify.packbools(unpadded),
            crumbify.packbools(padded),
            "False padding doesn't affect boolean packing."
        )

    def testpackbools_toomany(self):
        """Testing what happens when we provide too many bools for a byte."""
        self.assertRaises(RuntimeError, crumbify.packbools, [False] * 10)

if __name__ == "__main__":
    unittest.main()
