#!/bin/sh
cd $(dirname $(readlink -f "$0"))/../


#pydoctor works well even with existing dirs
#rm -r doc/source/.static/apidocs
pydoctor --add-package=code/breadcrumb/ --project-name="Breadcrumb" --make-html --html-output=doc/source/.static/apidocs

rm -r doc/source/.static/issues
ditz html doc/source/.static/issues

# Remove the build dir && build html + pdf
cd doc
make html
#make latex
#cd build/latex
#make 
#cd ../
cd ../../
