#!/usr/bin/env python
"""Updates the VERSION file based on the git tree."""

import os
import time
import logging

import git

def main():
    repo = git.Repo()
    branch = repo.git.branch().split()[1]

    if 'release' in branch:
        logging.info("Detected a release branch (%s)..." % branch)
        version = repo.git.tag()
    else: # nightly
        logging.info("Detected a nightly branch (%s)..." % branch)
        version = time.strftime("%Y%m%d")
        if branch != "master":
            logging.debug("Branch is not master, adding to version...")
            version = '%s-%s' % (version, branch)

    logging.info("Created version string (%s)..." % version)
    
    write_version(version)

def write_version(version):
    absdir = os.path.dirname(os.path.abspath(__file__))
    rootdir = os.path.dirname(absdir)
    breadcrumbdir = os.path.join(os.path.join(rootdir, 'code'), 'breadcrumb')
    versionfn = os.path.join(os.path.join(breadcrumbdir, 'common'), 'VERSION')

    logging.debug("Writing to version file (%s)..." % versionfn)

    versionfh = file(versionfn, 'w')
    versionfh.write(version)

if __name__ == "__main__":
    logging.getLogger().setLevel(logging.DEBUG)
    main()
