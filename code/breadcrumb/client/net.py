# -*- coding: utf8 -*-
"""Provides networking for breadcrumb, so it can talk to a server. """

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import collections

from crumbify import crumbify
from socket import socket, AF_INET, SOCK_DGRAM, SOCK_STREAM, gethostname
from common.consts import DEFAULT_PORTS

class Client():
    """A base class for clients."""
    def __init__(self,
                 host = None, port = None,
                 burst_length = 1):
        
        if host == None:
            host = gethostname()
        
        self.addr = host, port
        self.network_interface = None
 
        self.burst_length = burst_length
        self.empty_queue()
    
    def send_point(self, point):
        """Sends a point, optionally with signature.

        Calls the real send method as soon as the queue is full.
        """
        crumb = crumbify(point, ['timestamp', 'coordinates'])

        self.crumbs.append(crumb)

        if len(self.crumbs) >= self.burst_length:
            while self.crumbs:
                next = self.crumbs.popleft()
                self._send_crumb(next)

    def _send_crumb(self, crumb):
        """Sends a single crumb to the server."""       
        logging.debug("Sending crumb...")
        self.network_interface.send(crumb)

    def empty_queue(self):
        """Creates a new empty queue."""
        self.crumbs = collections.deque()

class UDPClient(Client):
    """A simple UDP socket client to talk to the breadcrumb UDP server."""
    def __init__(self, host = None, port = None):
        if port is None:
            port = DEFAULT_PORTS['UDP']

        Client.__init__(self, host = host, port = port)

        self.network_interface = socket(AF_INET, SOCK_DGRAM)
        self.network_interface.connect(self.addr)

        logging.info("New UDP client created, h:%s p:%s" % (host, port))
        
class TCPClient(Client):
    """A simple TCP socket client to talk to the breadcrumb TCP server."""
    def __init__(self, host = None, port = None):
        if port is None:
            port = DEFAULT_PORTS['TCP']
        
        Client.__init__(self, host = host, port = port)
        self.network_interface = socket(AF_INET, SOCK_STREAM)
        logging.info("New TCP client created, h:%s p:%s" % (host, port))
