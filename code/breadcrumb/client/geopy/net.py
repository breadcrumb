"""Provides networking for breadcrumb, so it can talk to a server.

author: Laurens Van Houtven <lvh at laurensvh.be>
license: GPLv3 or later
date: 25 Nov 2008 (file creation)
"""

from crumbify import crumbify 
from socket import socket, AF_INET, SOCK_DGRAM
from logging import debug, info

class Client():
    """ A base class for TCP and UDP clients. """
    def __init__(self, addr = ("localhost", 27862),
                 send_packets = True, burst_length = 1,
                 keep_stale_crumbs = False):
        
        self.addr = addr
        
        self.crumbs = []
        
        self.keep_stale_crumbs = keep_stale_crumbs
        self.stale_crumbs = []
        
        self.send_packets = send_packets
        self.burst_length = burst_length
        
        self.socket = None # Overwritten by subclass
        
    def send_data_point(self, data_point):
        """ Sends a data point, optionally with signature.
        
        Calls the real send method as soon as the queue is full. """
        crumb = crumbify(data_point)
        debug("New crumb made: '%s'" % crumb)
        
        self.crumbs.append(crumb)
        
        if len(self.crumbs) >= self.burst_length:
            if self.keep_stale_crumbs:
                self.stale_crumbs.append(self.crumbs)
            for crumb in self.crumbs:
                nextcrumb = self.crumbs.pop(0) # crumbs is a queue
                self.send_crumb(nextcrumb)
    
    def send_crumb(self, crumb):
        """ Sends a single crumb to the server. """       
        debug("Sending crumb...")
        self.socket.connect(self.addr)
        self.socket.send(crumb)
            
    def purge_queue(self):
        """ Purges the queue of crumbs that still have to be sent. """
        self.crumbs = []

class UDPClient(Client):
    """ A simple UDP socket client to talk to the breadcrumb UDP server. """
    def __init__(self, addr = ("localhost", 27862),
                 send_packets = True, burst_length = 1,
                 keep_stale_crumbs = False):
        
        Client.__init__(self, addr,
                        send_packets, burst_length,
                        keep_stale_crumbs)
        
        self.socket = socket(AF_INET, SOCK_DGRAM)
        info("New UDP client created, h:%s p:%s" % addr)
        debug("    ksc: %s, burst: %s" % (keep_stale_crumbs, burst_length))
        
class TCPClient(Client):
    """ A simple TCP socket client to talk to the breadcrumb TCP server. """
    def __init__(self, addr = ("localhost", 27862),
                 send_packets = True, burst_length = 1,
                 keep_stale_crumbs = False):
        
        Client.__init__(self, addr,
                        send_packets, burst_length,
                        keep_stale_crumbs)
        
        self.socket = socket(AF_INET, SOCK_DGRAM)
        
