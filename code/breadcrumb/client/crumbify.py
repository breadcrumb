# -*- coding: utf8 -*-
""" Takes a data point and encodes it into a crumb (short packed string). """

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import struct
import sys
import os

# import ../common
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from common.consts import CRUMB_FIELDS
from common.checksum import generate_byte_checksum

def crumbify(point, fields = zip(*CRUMB_FIELDS)[0]):
    """ Takes specified data from a data point and serializes it.
    
    The fields argument is optional, and should be a list of the attributes in
    the data point that have to be encoded. If unspecified, it tries to encode
    everything in the point.

    See the keys of the CRUMB_FIELDS dict in the common constants
    file for possible attribute names.
    """
    
    rawvalues = []
    formats   = ['<']

    # Header (unsigned char)
    rawvalues.append(createheaderbyte(fields))
    formats.append('B')

    # Optional fields:
    for field in CRUMB_FIELDS:
        attrname, attrkeys, attrformat = field
        if attrname in fields:
            # Do we have all the required keys?
            if all(key in point.data for key in attrkeys):
                formats.append(attrformat)
                rawvalues.extend([point.data[key] for key in attrkeys])
            else:
                ## TODO: Create header byte later so we can recover from this
                raise RuntimeError, 'Missing keys for encoding %s' % attrname

    format = ''.join(formats)
    logging.debug('Raw values: %s' % str(rawvalues))
    logging.debug('Format: %s, expsize: %d' % (format, struct.calcsize(format)))
    
    packed = struct.pack(format, *rawvalues)
    
    # Checksum byte:
    csum = generate_byte_checksum(packed)
    
    crumb = packed + csum
    logging.debug("Made crumb: %s" % ':'.join('%02X' % ord(c) for c in crumb))
    return packed + csum

def createheaderbyte(attrs):
    """ Creates the header byte for a crumb containing supplied attributes.
    
    Argument should be a list of attributes (strings).
    """
    return packbools([(field[0] in attrs) for field in CRUMB_FIELDS])

def packbools(bools):
    """ Packs up to eight boolean values in a single byte. """
    if len(bools) > 8:
        raise RuntimeError, 'Too many booleans (%d) for a byte!' % len(bools)

    return sum(1 << i for i, b in enumerate(bools) if b)

    #Alternative big-endian version (using a different method)
    #return struct.pack('b', int(''.join('1' if x else '0' for x in bools), 2))
