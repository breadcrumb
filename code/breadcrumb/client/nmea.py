# -*- coding: utf-8 -*-
""" Provides tools for parsing NMEA sentences. """

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import time
import logging
import sys
import os

from geopy.util import parse_geo
import geopy

from consts import SENTENCE_OFFSETS

# import ../common
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from common.checksum import is_valid_nmea_sentence

def parse(raw_sentence):
    """ Parses an NMEA sentence.

    Input should be a single NMEA sentence. Output is a dict of the relevant
    key, value pairs, suitable for creating a DataPoint object.

    Some GPSes keep spamming RMC/GGA sentences even though there is no fix.
    These sentences are entirely empty except for the timestamp and header.
        - example: the built-in GPS in my HTC Diamond
    
    Other GPSes only send RMC/GGA when they actually have a (reliable) fix,
    or send a lot less data, or send a bogus value for heading or velocity.
        - example: my HOLUX GPSlim236
    """
        
    sentence = raw_sentence.strip()
    logging.debug("Got sentence: %s" % sentence)

    if not interesting(sentence):
        logging.debug("Sentence was not interesting, ignoring...")
        return None 
        
    if not is_valid_nmea_sentence(sentence):
        return None

    # Extract data from the unpacked sentence
    unpacked = unpack(sentence)
    if unpacked is None:
        return None
    else:   
        return extract(unpacked)

def interesting(sentence):
    """ Determines if a sentence is interesting or not by looking at the header.

    A sentence is interesting if it starts with $GP:
    >>> interesting('$GP')
    True

    This includes most (probably even all) non-proprietary sentences.
    
    Returns false for all other sentences:
    >>> interesting('$XX')
    False
    """
    if sentence[0:3] == '$GP':
        return True
    else:
        return False 

def unpack(sentence):
    """ Returns the unpacked version of the sentence, minus header and checksum.

    >>> unpack('$GPGGA,spam,eggs*00')
    ['GGA', 'spam', 'eggs']
    """
    if sentence[-3] == "*": # sentence with checksum
        return sentence[3:-3].split(',')
    elif sentence[-1] == "*": # sentence without checksum
        return sentence[3:-1].split(',')
    else:
        logging.debug("Couldn't figure out how to split %s.." % sentence)
        return None

def extract(data):
    """
    Extracts data from a given array using given offsets.
    
    Returns a dictionary with the necessary data.
    """
    point_data = {}
    
    offsets = getoffsets(data)

    # Put the data from the sentence into a dict:
    for key, index in offsets: 
        if not data[index]:
            logging.debug("Partially empty sentence, attr: %s" % key)
            continue

        # Special cases:
        elif key == 'time':
            # Convert timestamp from NMEA (to unix) 
            point_data.update(timestamp = gettimestamp(data[index]))
        elif key == 'velocity_kt':
            # Convert velocity from knots (to m/s)
            point_data.update(velocity = float(data[index]) * 0.514)
        elif key == 'valid_rmc':
            if data[index] != 'A':
                logging.error("Bogus RMC sentence, ignoring...")
                return {}
        else:
            point_data.update({key: data[index]})
    
    # Get the coordinates, too:
    point_data = getcoordinates(point_data)
    return point_data

def getoffsets(data_array):
    """ Returns the right sentence offsets for a given data array. """
    sentence_type = data_array[0]
    
    if sentence_type in SENTENCE_OFFSETS:
        return SENTENCE_OFFSETS[sentence_type].items()
    else:
        logging.error("No offsets for sentence type %s..." % sentence_type)
        return {}.items() 

def gettimestamp(nmea_timestamp):
    """ Gets the number of seconds since epoch for a given NMEA timestamp.
    
    NMEA timestamps only contain the time for that day (24 hour format).

    This is turned into a UNIX time stamp (seconds since epoch). The date is
    guessed by picking the current date. Since this conversion is done on the
    client, and the latency between receiving and parsing is pretty small, this
    should not cause big problems.
    
    However, you might see serious problems if your GPS and your client don't
    agree on the current time. If the clock on the GPS is ahead of that the
    client and you're receiving points around midnight, your GPS will pretend
    the points are from just after midnight, but your PC will be convinced they
    are almost 24 hours ago (midnight today instead of midnight tomorrow).

    If this turns out to be a problem, we can always ignore the NMEA time
    stamp (either always, or some symmetric interval around midnight). This is
    not implemented yet.

    The argument is usually a string representation of an int, but passing an
    integer works equally well:
    """

    # TODO: Implement functionality to ignore the NMEA timestamp and just get
    # the client's timestamp.

    # Hours, minutes, seconds
    hrs  = int(nmea_timestamp[0:2])
    mins = int(nmea_timestamp[2:4])
    if '.' in nmea_timestamp[4:]: # NMEA timestamp with microseconds
        [raw_s, raw_ms] = nmea_timestamp[4:].split('.')
        secs = int(raw_s)
        ms = int(raw_ms)
    else: # NMEA timestamp without microseconds
        secs = int(nmea_timestamp[4:])
        ms   = 0
    
    # Year, month, day -- assumes we're getting recent data
    now = datetime.datetime.utcnow().timetuple()
    y, m, d = now[0], now[1], now[2]

    realtime = datetime.datetime(y, m, d, hrs, mins, secs, ms)
    
    return int(time.mktime(realtime.timetuple()))

def getcoordinates(data):
    """ Extract latitude and longitude from raw data.
    
    First, the ugly NMEA notation is turned into something prettier. Then, the
    geopy module is used to transform the decimal minutes notation into decimal
    degrees, which allows representing a coordinate with a single float.
    
    All of the source keys, which are basically the values from the sentence,
    are deleted once the pretty coordinates have been calculated.
    """

    keys = ['lat_fl', 'lat_ns', 'lon_ew', 'lon_fl']
    
    for key in keys:
        if key not in data:
            return data # Nothing for us to do here.

    latstr = _extract_lat_or_lon(data['lat_fl'], data['lat_ns'])
    lonstr = _extract_lat_or_lon(data['lon_fl'], data['lon_ew'])
        
    for key in keys:
        del data[key]

    latitude, longitude = geopy.util.parse_geo("%s, %s" % (latstr, lonstr))
    data.update(latitude = float(latitude), longitude = float(longitude))
        
    return data

def _extract_lat_or_lon(ugly_float, hemisphere):
    """ Parses the ugly NMEA notation for latitudes and longitudes.
    
    This returns a pretty string with degrees and decimal minutes:

    ugly_float is usually a string, since it comes from chopping NMEA sentences,
    which are strings. It describes a float (specifically, a single coordinate
    in degrees and decimal minutes), but it's encoded as a string.
    
    All angles are positive. The correct hemisphere determined by appending one
    of ('N','E','S','W'). The geopy module parses this later, resulting in a 
    two float value for coordinates (decimal degrees with signing determining
    hemisphere).

    Using a negative angle complains, but continues without the sign.

    This method does not check correctness of the string. It will return the
    most likely candidate (which will most likely still make geopy choke). This
    should never happen unless the GPS that produced the sentence is horribly
    broken (or simply malicious).
    """
    [left, right] = str(ugly_float).split('.')
    degrees = int(left[:-2])

    if degrees < 0:
        logging.error('Angle < 0 in coord %s, fixing...' % ugly_float)
        degrees = -degrees

    minutes = "%s.%s" % (left[-2:], right)
    return "%s� %s' %s" % (degrees, minutes, hemisphere)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
