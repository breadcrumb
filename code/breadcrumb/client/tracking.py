# -*- coding: utf8 -*-
"""Tracking code for Breadcrumb. Mainly glue between other components."""

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import net
import logging
import collections

class Tracker:
    """The central class for handling GPS tracking."""
    def __init__(self, host = None, port = None):
        self.network_interface = net.UDPClient(host = host, port = port)
        self.gps = None # See connect_to_gps()
        self.messages = collections.deque()
        self.last_point = None

    def start(self):        
        """Starts the main tracking cycle.
        
        This iterates over the gps attribute, which produces points.
        """
        for new_point in self.gps:
            if new_point is not None: # new point
                if self.last_point is None:
                    self.last_point = new_point

                elif self.last_point < new_point:
                    self.last_point.update(new_point)

                else: 
                    continue

                self.send_point()

        logging.debug("No more data from the GPS! Fake device?")
    
    def send_point(self):
        """Sends the tracker's current point.

        Also tries to send a handler message together with the point, unless
        that point already has one. In that case, a second crumb is sent
        immediately afterwards that has nothing but the handler message. This
        prevents clients that send a handler message with *every* point from
        blocking all other handler messages permanently.
        """
        delayed = False
        if self.messages:
            if not has_message(self.last_point):
                next_message(point)
            else:
                delayed = True

        self.network_interface.send_point(self.last_point)
        self.last_point.clear_message()

        if delayed:
            point = self.next_message()
            self.networkinterface.sendpoint(point)

    def next_message(self, point = {}):
        """Gets the next handler message from the queue and adds it to a point.

        Uses an empty dict if no point is provided.

        The point is always returned. This is useful if no point was provided.
        """
        if self.messages:
            id, msg = self.messages.popleft()
            point.update({'handlerid': id, 'handlermessage': msg})
        
        return point

def has_message(point):
    """Returns True if the point has a handler message, False otherwise."""
    for attr in ('handlerid', 'handlermessage'):
        if attr not in point: 
            return False
    return True
