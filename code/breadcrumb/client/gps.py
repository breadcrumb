# -*- coding: utf8 -*-
"""
Provides several GPS interfaces and means to create them.

These GPS interfaces are all iterable and return data points.
"""

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import time
import os

# NMEA parsing and data point generation
import nmea
import deltas

# Serial port interfaces
import ceserial.ceserial as ceserial
import serial

try:
    import gps
except ImportError:
    logging.debug("'gps' module not found, gpsd support disabled...")
    gps = None

# Default CE ports: try 8 and 4 first, those are usually the defaults for BT
# and raw serial GPSes, respectively. Also, yes, WinCE ports end in ':'.
DEFAULT_CE_PORTS = ['COM8:', 'COM4:', 'COM1:', 'COM2:', 'COM3:', 'COM5:']
# If we're not running on CE, let pyserial figure it out.
DEFAULT_GENERIC_PORTS = range(8)

DEFAULT_GPSD_HOSTNAME = 'localhost'
DEFAULT_GPSD_PORT = 2947

def connect(port = None,
            platform = None,
            host = None,
            sentence_filename = None):
    """Tries to connect to a GPS device.
    
    Calling this without any arguments tries to automagically detect everything.
    
    The platform is guessed based on the output of ``os.name``. On WinCE (aka
    Windows Mobile), it tries ceserial. In almost all other cases, it tries
    pyserial. Previously, ``sys.platform`` was used, but modern versions of 
    PythonCE return ``win32``, just like Python on Win2k/XP/Vista would.

    When this autodetection failed (and no serial port was opened), the client
    complains loudly, and we return a fake GPS with replayed sentences. Please
    note that the fact that this did not happen does not imply success -- just
    because you can open a port doesn't mean there's anything on it.
    
    The port usually refers to a serial port. Depending on the platform, this
    either looks a bit like "COM0" (Win32), "COM0:" (WinCE), or "/dev/ttyS0"
    (Linux)... 

    gpsd support requires explicitly setting the platform to ``'gpsd'``. The
    port then refers to a TCP port on which gpsd is listening (the default is
    is 2947). The hostname argument is the hostname on which gpsd is listening.
    
    The hostname argument is not used except for gpsd. This is because serial
    ports can generally only be accessed locally. Remote serial port access,
    although possible, is not supported, unless of course whatever it is you're
    using manages to disguise the remote port as a local one sufficiently well.
    """
    logging.debug('Trying to connect to a GPS (platform: %s)...' % platform)

    if platform == 'fake':
        return _fake_nmea_gps(sentence_filename)

    if platform == 'gpsd':
        return connect_to_gpsd(host, port)
    
    if platform is None:
        # sys.platform on recent versions of PythonCE is 'win32', just like on
        # normal Windows boxes. os.name returns 'ce' on WinCE.
        platform = os.name.lower()
        logging.debug("Detected platform %s..." % platform)

    serial_port = connect_to_serial(port, platform)

    if serial_port is not None:
        return nmeaGPS(serial_port)
    else:
        raise RuntimeError, "Host doesn't seem to have any serial ports?!"

def connect_to_serial(port = None, platform = 'ce'):
    """Attempts to connect to a serial GPS device. Returns a serial port.
    
    When forced to autodetect, the first serial port that can be opened will be
    picked. This might not be the wrong device -- the only way to fix that is
    to make sure the default ports are *likely* to be GPSes.

    Please note that a port might (usually: will) open even though there is no
    device connected to it.
    """

    logging.debug("Attempting to connect to a GPS device...")
    if port is None:
        if platform == 'ce':
            logging.debug('Using common defaults for WinCE...')
            potential_ports = DEFAULT_CE_PORTS
        else:
            logging.debug("Using common defaults for pyserial...")
            potential_ports = DEFAULT_GENERIC_PORTS
    else:
        logging.debug("Port specified: %s." % port)
        
        if platform == 'ce' and port[-1] is not ':':
            # WinCE does not agree with 99% of its users on how to name ports
            port = ''.join([port, ':'])

        potential_ports = [port]

    for portname in potential_ports:
        port = _try_serial_port(portname, platform)
        if port is not None:
            return port

    logging.error("Exhausted potential serial ports...")
    return None

def _try_serial_port(port, platform):
    """ Tries to connect to a GPS device on a serial given port. """
    
    logging.debug("Attempting to open serial port '%s'" % port)
    if platform is 'ce':
        gps = ceserial.CESerial(port = port)
    else:
        gps = serial.Serial(port)

    try:
        gps.open()
    except:
        logging.info("Couldn't open serial port '%s'." % port)
        return None
    
    logging.debug("Opening serial port %s successful..." )
    gps.sentence_type = 'nmea' # Assume NMEA.
    
    return gps

def _fake_nmea_gps(sentence_filename = None):
    """Returns a fake GPS device (NMEA sentence replayer)."""
    if sentence_filename is None:
        ownpath = os.path.dirname(os.path.abspath(__file__))
        filename = os.path.join(ownpath, 'sampletrip')
    else:
        filename = os.path.abspath(sentence_filename)
    
    filehandle = open(filename, 'r')
    
    logging.debug("Returning a fake GPS...")
    return nmeaGPS(filehandle)

def connect_to_gpsd(host = None, port = None):
    """ Connects to a gpsd daemon running on the specified hostname and port. """

    return gpsdGPS(host, port)

class nmeaGPS(object):
    """
    An interface for any source of NMEA sentences.
    
    The first argument must be an iterable object that returns NMEA sentences.
    This can be a list (for a fake GPS), a serial device (a real GPS)...
    """
    # TODO:Implement sentence logging in nmeaGPS
    def __init__(self, sentence_iterable):
        self.sentences = sentence_iterable
    
    def __iter__(self):
        for sentence in self.sentences:
            yield deltas.Point(nmea.parse(sentence))

    def close(self):
        """ Fake close() method."""
        pass

class gpsdGPS(object):
    """
    An interface to the gpsd_ daemon.
    
    .. _gpsd: http://gpsd.berlios.de/
    """
    def __init__(self, host = None, port = None):
        if port is None:
            self.port = DEFAULT_GPSD_PORT
        if host is None:
            self.host = DEFAULT_GPSD_HOSTNAME
        
        self.session = gps.gps(host, port)

    def __iter__(self):
        """ Gets the next data point from the gpsd daemon. """
        while True:
            self.session.query('adyos')

            data = {
                'latitude':  float(self.session.fix.latitude),
                'longitude': float(self.session.fix.longitude),
                'altitude':  float(self.session.altitude),
                'velocity':  float(self.session.speed),
                'timestamp': int(self.session.time),
            }

            yield deltas.Point(data)
