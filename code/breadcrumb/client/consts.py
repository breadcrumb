# -*- coding: utf8 -*-
""" Useful constants for Breadcrumb client-side programs.

Sentence offsets for parsing sentence strings
See http://www.gpsinformation.org/dale/nmea.htm for an explanation on all sorts
of NMEA sentences, not anywhere near limited to those used in this application.

SENTENCE_OFFSETS: The keys can be a bit terse, so here's the explanation:
  - lat_fl, lon_fl: floating point values for lat, lon (pretty raw)
  - lat_ns, lon_ew: hemisphere (north/south for lat, east/west for lon)
  - time: self-explanatory
  - velocity, velocity_kt: velocity in m/s, knots
  - heading: true heading
  - m_var: magnetic variation
  - m_var_dir: direction of magnetic variation, set(['E', 'W'])
"""

DEFAULT_THRESHOLDS = ( # Push a new point when...
    ('heading',   15),  # ... we turn 15 degrees or more,
    ('distance',  500),  # ... or we travel 500 meters or more,
    ('timestamp', 1200),  # ... or twenty minutes pass (heartbeat).
)

SENTENCE_OFFSETS = {
    'GGA': {
        'time':   1,
        'lat_fl': 2,
        'lat_ns': 3,
        'lon_fl': 4,
        'lon_ew': 5,
    },
    
    'GGL': {
        'time':   5,
        'lat_fl': 1,
        'lat_ns': 2,
        'lon_fl': 3,
        'lon_ew': 4,
    },
    
    'RMC': {
        'time':   1,
        'lat_fl': 3,
        'lat_ns': 4,
        'lon_fl': 5,
        'lon_ew': 6,
        
        'valid_rmc':   2,
        'velocity_kt': 7,
        'heading':     8,
        'm_var':       9,
        'm_var_dir':   10,
    },
}
