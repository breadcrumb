# -*- coding: utf8 -*-
""" Handles data points and differences (deltas) between them. """

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import consts
import logging
import geopy


class Point:
    """A container for GPS data.
    
    Provides data scrubbing for GPS data and syntactic sugar for comparison:
    p1 < p2 means that the deltas between p1 and p2 are significant (at least
    one delta bigger than the threshold).
    """
    thresholds = consts.DEFAULT_THRESHOLDS
    
    def __init__(self, data):
        self.data = data 

    def update(self, other):
        """Updates the data in this point with data from a new point."""
        self.data.update(other.data)
   
    def clear_message(self):
        """Clears the handler message."""
        
        for key in ['handlerid', 'handlermessage']:
            if key in self.data:
                del self.data[key]

    def __getitem__(self, key):
        """Gets an entry from the point's dict."""
        try:
            return self.data['key']
        except KeyError:
            return None

    def __setitem__(self, key, value):
        """Sets an entry in the point's dict."""
        self.data[key] = value

    def __delitem__(self, key):
        """Deletes an entry from the point's dict."""
        del self.data[key]

    def __lt__(self, other):
        """Decides if a point is significantly different from another."""
        for threshold_name, threshold_value in Point.thresholds:
            difference = delta(self, other, threshold_name)
            if not difference:
                logging.debug("Tried threshold %s, couldn't compare..."
                % threshold_name )
                continue 
            elif difference < threshold_value:
                logging.debug("Didn't pass threshold %s (diff=%2f, thresh=%2f)"
                % (threshold_name, difference, threshold_value))
                continue
            else: # threshold passed!
                logging.debug("Passed threshold %s (diff=%2f, thresh=%2f)"
                % (threshold_name, difference, threshold_value))
                return True 

        return False # Thresholds exhausted, none triggered 

    def __gt__(self, other):
        raise NotImplementedError

def delta(p1, p2, attribute):
    """Returns the difference in attribute between this point and another.
    
    Differences are returned in SI units (seconds, meters...), except for
    any angle (usually, this means true heading). Angles are returned in
    degrees (because using radians would just be plain silly).

    Equal points should always result in 0.0 (a float).

    The distance code uses geopy's distance.distance function to calculate
    the Vincenty distance, even though this is not really neccesary, since
    sequential points generally aren't too far apart.

    The heading code might look a bit strange at first. It accounts for the
    fact that headings are cyclic: 359.0 and 1.0 are very close together and
    should return a delta of 2.0, not |359.0 - 1.0| = 358.0.

    This potentially raises issues when making U-turns, when the angle really
    is ~180 degrees. This should be fixed by not making the heading threshold
    too conservative, leading to several points that form the U-turn.
    
    Returns None on failure.
    """
    
    # Shorthand: 
    get = lambda attr: get_attr_as_floats(p1, p2, attr)

    if attribute == 'distance':
        try:
            lat1, lat2 = get('latitude')
            lon1, lon2 = get('longitude')
        except RuntimeError:
            return

        return geopy.distance.distance((lat1, lon1), (lat2, lon2)).km * 1000
        
    elif attribute == 'heading':
        try:
            h1, h2 = get('heading')
            delta_heading = abs(h1 - h2)
        except RuntimeError:
            return

        if delta_heading < 180.0:
            return delta_heading
        else:
            return 360.0 - delta_heading
        
    else: # Generic case: dv = |v2 - v1|
        try:
            v1, v2 = get(attribute) 
        except RuntimeError:
            return
        
        delta_attr = abs(v2 - v1)

        if attribute != "timestamp":
            return delta_attr
        else:
            return int(delta_attr) 

def get_attr_as_floats(p1, p2, attribute):
    """Gets attributes from two data points as floats.
    
    These attributes might be in string form depending on where they come
    from (different sentences encode differently, unfortunately). This does
    not incur any real penalty if the data already happens to be a float.
    """
    if attribute not in p1.data or attribute not in p2.data:
        raise RuntimeError, 'Incomplete data'

    v1, v2 = p1.data[attribute], p2.data[attribute]
   
    return v1, v2 

# XXX: I'm not entirely sure if anyone uses this... - lvh
def _default_thresholds():
    """Gets the default thresholds from the consts module."""
    reload(consts)
    return consts.DEFAULT_THRESHOLDS

def update_tresholds(new_thresholds = None):
    """Updates the thresholds with the threshold data in the argument.
    
    If no argument is supplied, it uses the default values.

    Updating with anything else that is considered False by the interpreter
    will reset the dicts to the default value. This includes the default value,
    but also empty dictionaries. If an empty dict is passed, it will complain
    loudly, because that might not be expected behaviour.

    This will cause the dict to be updated, not replaced. Old thresholds will
    not be overwritten. (If you want this instead, see set_thresholds.)
    """
    if not new_thresholds:
        logging.error("Updating with empty dict, resetting thresholds...")

    thresholds.update(new_thresholds or _default_thresholds())

def set_thresholds(new_thresholds = None):
    """Sets the thresolds.
    
    If no argument is supplied, it uses the default values.
    
    Trying to set the thresholds to anything that evaluates to False also sets
    them to the default values. This includes the default argument, but also an
    empty dictionary.

    If the thresholds were an empty dict, that would effectively mean no points
    were ever considered important and no more points were sent. That's a very
    bad (opaque) way of turning the tracker off.
    """
    thresholds = new_thresholds or _default_thresholds()
