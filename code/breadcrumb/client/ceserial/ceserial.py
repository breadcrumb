# -*- coding: utf8 -*-
"""Provide serial port access to Windows CE devices.

Copyright:
Ben McBride
benjamin.mcbrideATgmail.com

Readline section ported from pyserial by Alex Mandel
techATwildintellect.com

Pyserial under the Python License
http://pyserial.svn.sourceforge.net/viewvc/pyserial/trunk/pyserial/LICENSE.txt

Cleanups and minor changes by Laurens Van Houtven <lvh@laurensvh.be>
"""
import winbase
from ctypes import *

PARITY_NONE = winbase.NOPARITY
PARITY_EVEN = winbase.EVENPARITY
PARITY_ODD = winbase.ODDPARITY

STOPBITS_ONE = winbase.ONESTOPBIT
STOPBITS_TWO = winbase.TWOSTOPBITS

FIVEBITS = 5
SIXBITS = 6
SEVENBITS = 7
EIGHTBITS = 8

XON = chr(17)
XOFF = chr(19)

#-----------------------------------------------------------
# Helper functions
#-----------------------------------------------------------
def bitmask(b):
    """Return a mask for setting a bit"""
    return (1 << b)

def bitset(value, b):
    """Set the specified bit of value"""
    return (value | bitmask(b))

def bitclear(value, b):
    """Clear the specified bit of value"""
    return (value & ~(bitmask(b)))

def bittest(value):
    """Return True if the bit of value is 1, False otherwise"""
    return 1 == ((value >> 1) & 1)

#-----------------------------------------------------------
# Windows Comm Structures
#-----------------------------------------------------------
class DCB(Structure):
    """Device Control Block structure"""
    _fields_ = [
        ("DCBlength", c_uint),
        ("BaudRate", c_uint),
        ("flags", c_uint),
# codervw:bdm      :1 ("fBinary", c_uint),
# codervw:bdm      :1 ("fParity", c_uint),
# codervw:bdm      :1 ("fOutxCtsFlow", c_uint),
# codervw:bdm      :1 ("fOutxDsrFlow", c_uint),
# codervw:bdm      :2 ("fDtrControl", c_uint),
# codervw:bdm      :1 ("fDsrSensitivity", c_uint),
# codervw:bdm      :1 ("fTXContinueOnXoff", c_uint),
# codervw:bdm      :1 ("fOutX", c_uint),
# codervw:bdm      :1 ("fInX", c_uint),
# codervw:bdm      :1 ("fErrorChar", c_uint),
# codervw:bdm      :1 ("fNull", c_uint),
# codervw:bdm      :2 ("fRtsControl", c_uint),
# codervw:bdm      :1 ("fAbortOnError", c_uint),
# codervw:bdm      :17("fDummy2", c_uint),
        ("wReserved", c_ushort),
        ("XonLim", c_ushort),
        ("XoffLim", c_ushort),
        ("ByteSize", c_byte),
        ("Parity", c_byte),
        ("StopBits", c_byte),
        ("XonChar", c_char),
        ("XoffChar", c_char),
        ("ErrorChar", c_char),
        ("EofChar", c_char),
        ("EvtChar", c_char),
        ("wReserved1", c_ushort)
        ]

class COMMTIMEOUTS(Structure):
    """Comm timeout structure"""
    _fields_ = [
        ("ReadIntervalTimeout", c_uint),
        ("ReadTotalTimeoutMultiplier", c_uint),
        ("ReadTotalTimeoutConstant", c_uint),
        ("WriteTotalTimeoutMultiplier", c_uint),
        ("WriteTotalTimeoutConstant", c_uint)
        ]

class COMSTAT(Structure):
    """Communication device data"""
    _fields_ = [
        ("flags", c_uint),
        ("cbInQue", c_uint),
        ("cbOutQue", c_uint)
        ]
    
#-----------------------------------------------------------
# Classes
#-----------------------------------------------------------
class SerialException(Exception):
    pass

class FileLike(object):
    """An abstract file like class.
    
    This class implements readline and readlines based on read and
    writelines based on write.
    This class is used to provide the above functions for to Serial
    port objects.
    
    Note that when the serial port was opened with _NO_ timeout that
    readline blocks until it sees a newline (or the specified size is
    reached) and that readlines would never return and therefore
    refuses to work (it raises an exception in this case)!
    """

    def read(self, size): raise NotImplementedError
    def write(self, s): raise NotImplementedError

    def readline(self, size=None, eol='\n'):
        """read a line which is terminated with end-of-line (eol) character
        ('\n' by default) or until timeout"""
        line = ''
        while 1:
            c = self.read(1)
            if c:
                line += c   #not very efficient but lines are usually not that long
                if c == eol:
                    break
                if size is not None and len(line) >= size:
                    break
            else:
                break
        return line

    def readlines(self, sizehint=None, eol='\n'):
        """read a list of lines, until timeout
        sizehint is ignored"""
        if self.timeout is None:
            raise ValueError, "Serial port MUST have enabled timeout for this function!"
        lines = []
        while 1:
            line = self.readline(eol=eol)
            if line:
                lines.append(line)
                if line[-1] != eol:    #was the line received with a timeout?
                    break
            else:
                break
        return lines

    def xreadlines(self, sizehint=None):
        """just call readlines - here for compatibility"""
        return self.readlines()

    def writelines(self, sequence):
        for line in sequence:
            self.write(line)

    def flush(self):
        """flush of file like objects"""
        pass

    # iterator for e.g. "for line in Serial(0): ..." usage
    def next(self):
        line = self.readline()
        if not line: raise StopIteration
        return line

    def __iter__(self):
        return self
		
class Serial(FileLike):
    """Serial port class"""
    _baudrates = {
        110 : winbase.CBR_110,
        300 : winbase.CBR_300,
        600 : winbase.CBR_600,
        1200 : winbase.CBR_1200,
        2400 : winbase.CBR_2400,
        4800 : winbase.CBR_4800,
        9600 : winbase.CBR_9600,
        14400 : winbase.CBR_14400,
        19200 : winbase.CBR_19200,
        38400 : winbase.CBR_38400,
        56000 : winbase.CBR_56000,
        57600 : winbase.CBR_57600,
        115200 : winbase.CBR_115200,
        128000 : winbase.CBR_128000,
        256000 : winbase.CBR_256000
        }

    def __init__(self, 
            port='COM7:', 
            baudrate=4800, 
            bytesize=8,
            stopbits=STOPBITS_ONE,
            parity=PARITY_NONE,
            timeout=5,
            xonxoff=0,
            rtscts=False
            ):
        self.__handle = winbase.INVALID_HANDLE_VALUE
        self._port = unicode(port) #CreateFileW requires wide character data
        self._baudrate = self._baudrates[baudrate]
        self._bytesize = bytesize
        self._stopbits = stopbits
        self._parity = parity
        self._timeout = timeout
        self._xonxoff = xonxoff
        self._rtscts = rtscts

    def open(self):
        """Try to open the serial port.

        Raises a SerialException exception if there are problems.
        """
        #check to see if the port is already open
        if self.isOpen(): return
        #get file handle for the COMM port
        self.__handle = windll.coredll.CreateFileW(
                            self._port,
                            winbase.GENERIC_READ | winbase.GENERIC_WRITE,
                            None,
                            None,
                            winbase.OPEN_ALWAYS,
                            None,
                            None
                            )
        if self.__handle == winbase.INVALID_HANDLE_VALUE:
            raise SerialException, windll.coredll.GetLastError()
        #---------------------------------------------------
        # set COMM port attributes
        #---------------------------------------------------
        #set input and output buffer sizes
        if not(windll.coredll.SetupComm(self.__handle, 4096, 4096)):
            raise SerialException, windll.coredll.GetLastError()
        try:
            self._configurePort()
        except SerialException, e:
            self.close()    #try to close the already open port
            raise SerialException, e

    def close(self):
        """Close the serial port.

        Raises a SerialException exception if there are problems.
        """
        if not(self.isOpen()): return
        if not(windll.coredll.CloseHandle(self.__handle)):
            raise SerialException, windll.coredll.GetLastError()
        self.__handle = winbase.INVALID_HANDLE_VALUE

    def read(self, size=1):
        """Read bytes from the serial port. The port needs to be opened
        before this function is called.
    
        Raises a SerialException exception if there are problems.
        """
        stats = self.getCommStats()

        data = ''
        nmbr_of_bytes_to_read = c_uint(size)
        nmbr_of_bytes_read = c_uint()
        buf = create_string_buffer(nmbr_of_bytes_to_read.value)
        if not(windll.coredll.ReadFile(
                        self.__handle,
                        buf,
                        nmbr_of_bytes_to_read,
                        byref(nmbr_of_bytes_read),
                        None
                        )):
            raise SerialException, windll.coredll.GetLastError()
        data += buf.raw[0:nmbr_of_bytes_read.value]
        return data
    
    def write(self, data):
        """Write data to the serial port. The port needs to be opened
        before this function is called.

        Raises a SerialException exception if there are problems.
        """
        nmbr_of_bytes = c_uint(len(data))
        bytes_written = c_uint()
        success = windll.coredll.WriteFile(
                        self.__handle,
                        data,
                        nmbr_of_bytes,
                        byref(bytes_written),
                        None
                        )
        if not(success):
            raise SerialException, windll.coredll.GetLastError()
    
    def isOpen(self):
        """Return True if the serial port is open"""
        return (self.__handle != winbase.INVALID_HANDLE_VALUE)

    def inWaiting(self):
        """Return the number of characters in the received queue"""
        comstats = self.getCommStats()
        return comstats.cbInQue
    
    def flushInput(self):
        """Flush input buffer, discarding contents"""
        flags = winbase.PURGE_RXABORT | winbase.PURGE_RXCLEAR
        if not(windll.coredll.PurgeComm(self.__handle, flags)):
            raise SerialException, windll.coredll.GetLastError()

    def flushOutput(self):
        """Flush output buffer, abort output"""
        flags = winbase.PURGE_TXABORT | winbase.PURGE_TXCLEAR
        if not(windll.coredll.PurgeComm(self.__handle, flags)):
            raise SerialException, windll.coredll.GetLastError()

    #-------------------------------------------------------
    # Non PySerial methods
    #-------------------------------------------------------
    def getDCB(self):
        """Get the DCB structure"""
        dcb = DCB()
        if not(windll.coredll.GetCommState(self.__handle, byref(dcb))):
            raise SerialException, windll.coredll.GetLastError()
        return dcb

    def getCommTimeouts(self):
        """Get the comm timeouts."""
        timeouts = COMMTIMEOUTS()
        if not(windll.coredll.GetCommTimeouts(self.__handle, byref(timeouts))):
            raise SerialException, windll.coredll.GetLastError()
        return timeouts

    def getCommStats(self):
        """Get the comm stats function as well as error flags"""
        comstats = COMSTAT()
        errors = c_uint()
        if not(windll.coredll.ClearCommError(
                        self.__handle,
                        byref(errors),
                        byref(comstats),
                        )):
            raise SerialException, windll.coredll.GetLastError()
        return comstats, errors.value
        
    def _configurePort(self):
        """Configure the port properties and set timeouts."""
        #set comm port timeout
        timeouts = self.getCommTimeouts()
        if self._timeout is None:
            timeouts.ReadIntervalTimeout = 0
            timeouts.ReadTotalTimeoutMultiplier = 0
            timeouts.ReadTotalTimeoutConstant = 0
        elif self._timeout == 0:
            timeouts.ReadIntervalTimeout = winbase.MAXDWORD
            timeouts.ReadTotalTimeoutMultiplier = 0
            timeouts.ReadTotalTimeoutConstant = 0
        else:
            timeouts.ReadIntervalTimeout = 0
            timeouts.ReadTotalTimeoutMultiplier = 0
            timeouts.ReadTotalTimeoutConstant = int(self._timeout*1000)
        timeouts.WriteTotalTimeoutMultiplier = 0
        timeouts.WriteTotalTimeoutConstant = 0
        if not(windll.coredll.SetCommTimeouts(self.__handle, byref(timeouts))):
            raise SerialException, windll.coredll.GetLastError()
        #get current port attributes
        dcb = self.getDCB()
        #set attributes
        dcb.BaudRate = self._baudrate
        dcb.ByteSize = self._bytesize
        dcb.StopBits = self._stopbits
        #these fields need flags (in the bitfield) to be set
        dcb.Parity = self._parity
        if self._parity == PARITY_NONE:
            dcb.flags = bitclear(dcb.flags, 1)
        else:
            dcb.flags = bitset(dcb.flags, 1)
        if self._xonxoff:
            dcb.flags = bitset(dcb.flags, 8)
            dcb.flags = bitset(dcb.flags, 9)
            dcb.XonChar = XON
            dcb.XoffChar = XOFF
        else:
            dcb.flags = bitclear(dcb.flags, 8)
            dcb.flags = bitclear(dcb.flags, 9)
        if self._rtscts:
            #set fRTSControl to winbase.RTS_CONTROL_HANDSHAKE
            dcb.flags = bitclear(dcb.flags, 12)
            dcb.flags = bitset(dcb.flags, 13)
            dcb.flags = bitset(dcb.flags, 2)
        else:
            dcb.flags = bitclear(dcb.flags, 2)
        #set new attributes
        if not(windll.coredll.SetCommState(self.__handle, byref(dcb))):
            raise SerialException, windll.coredll.GetLastError()

    #-------------------------------------------------------
    # Property methods
    #-------------------------------------------------------
    #TODO -- what happens when you set the port? PySerial changes on the fly
    def _getPort(self):
        return self._port

    def _setPort(self, p):
        self._port = unicode(p)
    port = property(_getPort, _setPort)

    def _getBaudrate(self):
        return self._baudrate

    def setBaudrate(self, baudrate):
        """Named this on purpose because PySerial makes this
        function public, the same thing can be accomplished
        through setting the baudrate attribute though.
        """
        self._baudrate = self._baudrates[baudrate]
        if self.isOpen():
            self._configurePort()
    baudrate = property(_getBaudrate, setBaudrate)

    def _getBytesize(self):
        return self._bytesize

    def _setBytesize(self, size):
        self._bytesize = size
        if self.isOpen():
            self._configurePort()
    bytesize = property(_getBytesize, _setBytesize)

    def _getParity(self):
        return self._parity
    
    def _setParity(self, parity):
        self._parity = parity
        if self.isOpen():
            self._configurePort()
    parity = property(_getParity, _setParity)

    def _getStopbits(self):
        return self._stopbits

    def _setStopbits(self, stopbits):
        self._stopbits = stopbits
        if self.isOpen():
            self._configurePort()
    stopbits = property(_getStopbits, _setStopbits)

    def _getTimeout(self):
        return self._timeout

    def _setTimeout(self, t):
        if self._timeout != t:
            self._timeout = t
            if self.isOpen():
                self._configurePort()
    timeout = property(_getTimeout, _setTimeout)

    def _getXonXoff(self):
        return self._xonxoff

    def _setXonXoff(self, x):
        self._xonxoff = x
        if self.isOpen():
            self._configurePort()
    xonxoff = property(_getXonXoff, _setXonXoff)

    def _getRtsCts(self):
        return self._rtscts

    def _setRtsCts(self, rtscts):
        self._rtscts = rtscts
        if self.isOpen():
            self._configurePort()
    rtscts = property(_getRtsCts, _setRtsCts)

    #-------------------------------------------------------
    # Read only attributes
    #-------------------------------------------------------
    def _getPortstr(self):
        return str(self.port)
    portstr = property(_getPortstr)

    def _getBaudrates(self):
        return Serial._baudrates.keys()
    BAUDRATES = property(_getBaudrates)

    def _getParities(self):
        return [PARITY_NONE, PARITY_EVEN, PARITY_ODD]
    PARITIES = property(_getParities)

    def _getBytesizes(self):
        return [FIVEBITS, SIXBITS, SEVENBITS, EIGHTBITS ]
    BYTESIZES = property(_getBytesizes)

    def _getStopbits(self):
        return [STOPBITS_ONE, STOPBITS_TWO]
    STOPBITS = property(_getStopbits)

    #-------------------------------------------------------
    # XXX these functions are not implemented, 
    #     but are defined by pySerial
    #-------------------------------------------------------

    def sendBreak(self):
        raise NotImplementedError

    def setRTS(self, level=1):
        raise NotImplementedError

    def setDTR(self, level=1):
        raise NotImplementedError

    def getCTS(self):
        raise NotImplementedError

    def getDSR(self):
        raise NotImplementedError

    def getRI(self):
        raise NotImplementedError

    def getCD(self):
        raise NotImplementedError

