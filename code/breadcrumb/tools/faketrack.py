# -*- coding: utf8 -*-
"""Generates fake tracks."""

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import optparse

import datetime
import math
import random

import logging
import os 
import sys

# "import ../common.checksum"
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from common.checksum import generate_hex_checksum

# Base unit vectors
LEFT  = (-1,  0)
RIGHT = ( 1,  0)
UP    = ( 0,  1)
DOWN  = ( 0, -1)

# Direction sequences
DIRECTION_SEQUENCES = {
    'left_cw':    (LEFT,  UP,   RIGHT, DOWN),
    'left_ccw':   (LEFT,  DOWN, RIGHT, UP  ),
    'right_cw':   (RIGHT, DOWN, LEFT,  UP  ),
    'right_ccw':  (RIGHT, UP,   LEFT,  DOWN),
    'random':     None,
    'random180':  None,
}

# Preseeded hop length cache:
cached = {
    'fibonacci':   [1, 1],
    'linear':      [1, 1],
    'lucas':       [2, 1],
    'logarithmic': [],
    'random':      [],
}

# const globals
starttime = datetime.datetime.now()
onesecond = datetime.timedelta(seconds = 1)

def main():
    """Generates a fake track."""
    
    opts = getopt()

    outfile = None
    if opts.outfile:
        if opts.append:
            outfile = open(opts.outfile, 'a')
        else:
            outfile = open(opts.outfile, 'w')
    
    if opts.verbose:
        logging.getLogger().setLevel(logging.DEBUG)
    
    directions = DIRECTION_SEQUENCES[opts.directions]
    
    oldvec = (opts.latitude, opts.longitude) 
    resultvecs = [oldvec]

    lastbasevec = None

    for index in range(opts.numpoints):
        logging.debug("Index is %d." % (index + 1))
        
        baselength = nexthoplength(index, opts.sequence)
        hoplength = baselength * opts.scale
        logging.debug("Next hoplength is %d." % hoplength)

        # basevec, scaledvec are relative to the last point
        basevec = nextdirection(opts.directions,
                                directions,
                                index,
                                lastbasevec)
        lastbasevec = basevec
        scaledvec = scale(basevec, hoplength)

        # newvec is relative to center (absolute)
        newvec = add(scaledvec, oldvec) 

        logging.debug("Calculated new vector: %s" % str(newvec))

        if opts.ip_distance != 0.0: # distance is set
            numips = vectornorm(substract(oldvec, newvec))/opts.ip_distance
        else: 
            numips = opts.ip_points

        relippoints = interpolate(oldvec, newvec, numips, opts.ip_method)
        ippoints = [add(oldvec, relpt) for relpt in relippoints]

        oldvec = newvec
        ippoints.append(newvec)
        
        for vec in ippoints:
            if not opts.nmea:
                #outstr = str(vec)
                outstr = "(%+5f, %+5f)" % vec
            else:
                outstr = nmea(index, vec)
            
            if outfile:
                outfile.write(outstr + "\n")
            else:
                print outstr
        
        resultvecs.extend(ippoints)

    logging.debug("Finished calculating vectors.")

    if opts.plot:
        import pylab

        [x, y] = zip(*resultvecs)
        pylab.plot(x, y, 'r')
        pylab.show()

def add(vec1, vec2):
    """Adds two vectors."""
    return tuple((x1 + x2) for (x1, x2) in zip(vec1, vec2))

def substract(vec1, vec2):
    """Substracts two vectors."""
    return tuple((x1 - x2) for (x1, x2) in zip(vec1, vec2))

def scale(vec, s):
    """Scales a vector with a given scalar value."""
    return tuple(x*s for x in vec)

def nextdirection(direction, directions, index, lastbasevec):
    """Gets the next direction."""
    if direction == 'random':
        return newrandomunitvec()
    
    elif direction == 'random180':
        if lastbasevec is None:
            return newrandomunitvec()
        return newrandom180unitvec(lastbasevec)
    
    else:
        return directions[index % len(directions)]

def nexthoplength(index, sequence):
    """Gets the next hop length."""
    next = None

    while index >= len(cached[sequence]):
        if sequence in 'linear':
            num = cached[sequence][-1] + 1
            cached[sequence].extend([num] * 2)
            next = num
        elif sequence in 'fibonacci' or sequence in 'lucas':
            next = cached[sequence][-1] + cached[sequence][-2]
            cached[sequence].append(next)
        elif sequence in 'logarithmic':
            next = math.log(index + 2)
            break
        elif sequence in 'random':
            next = random.randint(1,10)
            break
    if next:
        return next
    else:
        return cached[sequence][index]

def interpolate(v1, v2, n, method = 'linear'):
    """Interpolates between two given vector with a number of points."""
    dv = substract(v2, v1)
    if method == 'linear':
        scales = [(i+1)/float(n+1) for i in xrange(int(n))]
    if method == 'random':
        scales = sorted([random.random() for x in xrange(int(n))])
    
    return [scale(dv, q) for q in scales]

def newrandomunitvec():
    """Generates a random 2D unit vector."""
    a, b = 1 - 2*random.random(), 1 - 2*random.random()

    # You can do this in a different way too:
    # a = 1 - 2*random.random()
    # b = +/- sqrt(1 - a**2)
    # You still need to randomly pick the sign of b. Both values give you a
    # unit vector -- just mirrored, similar to a complex conjugate pair.

    return normalized((a, b))

def vectornorm(vector, norm = 2):
    """Returns the vector norm (default: 2-norm) of a given vector."""
    return math.pow(sum(x**norm for x in vector), 1/float(norm))

def normalized(vector, norm = 2):
    """Returns the normalized form of the vector."""
    return tuple([x/vectornorm(vector) for x in vector])

def newrandom180unitvec(lastbasevec, fuzzyfactor = 2):
    """Creates a new unit vector with a similar direction as the last one."""
    newvec = newrandomunitvec()
    newvec = scale(newvec, 1/float(fuzzyfactor))

    basevec = add(newvec, lastbasevec) 

    return normalized(basevec)

def nmea(index, vec):
    """Returns the latitude, longitude pair as an NMEA GGA sentence."""
    latitude, longitude = vec

    ts = "%02i%02i%02i" % (starttime + index * onesecond).timetuple()[3:6]
    #    |zero padding|    |old timestamp + one second|  |hrs, mins, secs|               
    lat_str = create_ugly_string(latitude,  'lat')
    lon_str = create_ugly_string(longitude, 'lon')

    nmea = "$GPGGA,%s,%s,%s,1,01,0,0,M,0,M,," % (ts, lat_str, lon_str)
    
    checksum = generate_hex_checksum(nmea[1:])
    nmea += "*%s" % checksum

    logging.debug("Created NMEA string: %s"% nmea)
    return nmea

def create_ugly_string(coordinate, coordtype):
    """Generates an NMEA representation of a coordinate.
    
    coordtype must be one of ('lat', 'lon')."""
    if coordtype == 'lat':
        # ceil(log(max_latitude, 10)) = 2
        # ceil(log(max_minutes,  10)) = 2
        # ceil(log(max_decmins,  10)) = 3
        if coordinate > 0:
            basestr = "%02i%02i.%03i,N" 
        else:
            basestr = "%02i%02i.%03i,S"
    elif coordtype == 'lon':
        # ceil(log(max_longitude, 10)) = 3
        # ceil(log(max_minutes,   10)) = 2
        # ceil(log(max_decmins,   10)) = 3
        if coordinate > 0:
            basestr = "%03i%02i.%03i,E"
        else:
            basestr = "%03i%02i.%03i,W"
   
    # We have hemisphere data now, so discard the sign
    coordinate = abs(coordinate)
    degrees = int(math.floor(coordinate))

    decdegs = coordinate - degrees # Fraction of a degree, ie 0.5 => 30 mins
    decmins = decdegs * 60
    
    minutes = int(math.floor(decmins))
    decmins = decmins - minutes # Fractions of a minute
    thousanths = int(math.floor(decmins * 1000))

    coord_string = basestr % (degrees, minutes, thousanths)

    logging.debug("Created coord string: %s" % coord_string)
    return coord_string

def getopt():
    description = """Generates a sequence of geodesic points.""" 
    parser = optparse.OptionParser(description = description,
                                   epilog = get_epilog())

    parser.add_option('-n', '--num-points', dest='numpoints',
                      type = 'int', default = 10, metavar = 'N',
                      help = 'Number of points in the sequence.')
    parser.add_option('-s', '--scale', dest='scale',
                      type = 'float', default = 1.0, metavar = 'S',
                      help = "Vector scale. ('1' means 1 unit is 1 degree).")
    parser.add_option('', '--file', dest='outfile',
                      help = 'If specified, writes output to FILE.',
                      default = None, metavar = 'FILE')
    parser.add_option('-a', '--append', dest = 'append', action = 'store_true',
                      default = False,
                      help = 'Appends the output file instead of overwriting.')
    parser.add_option('', '--latitude', dest='latitude',
                      type = 'float', default = 0.0, metavar = 'LAT',
                      help = 'Latitude of the center.')
    parser.add_option('', '--longitude', dest='longitude',
                      type = 'float', default = 0.0, metavar = 'LON',
                      help = 'Longitude of the center.')
    parser.add_option('', '--sequence', dest='sequence',
                      default = 'linear', metavar = 'SEQ',
                      help = 'Sets the hop length sequence. (see below)')
    parser.add_option('-N', '--nmea', dest = 'nmea', action = "store_true",
                      default = False,
                      help = "Creates fake NMEA sentences instead of tuples.")
    parser.add_option('', '--directions', dest = 'directions',
                      default = 'left_cw', metavar = 'DIR',
                      help = "Sets the direction sequence (see below).")
    parser.add_option('-i', '', dest = 'ip_points',
                      type = 'float', default = 0.0, metavar = 'N',
                      help = 'The number of interpolation points.')
    parser.add_option('-d', '', dest = 'ip_distance',
                      type = 'float', default = 0.0, metavar = 'D',
                      help = 'Average distance between interpolated points.')
    parser.add_option('-m', '', dest = 'ip_method',
                      default = 'linear', metavar = 'METHOD',
                      help = 'The method used for interpolation.')
    parser.add_option('-p', '--plot', dest = 'plot', action='store_true',
                      default = False,
                      help = 'Display a plot when done.')
    parser.add_option('-v', '--verbose', dest='verbose', action='store_true',
                      default = False,
                      help = 'Enable debugging messages.')

    return parser.parse_args()[0]

def get_epilog():
    strs = [
    "Known hop length sequences: "    + ", ".join(cached.keys()),
    "Known direction sequences: " + ", ".join(DIRECTION_SEQUENCES.keys()),
    "All angles are in decimal degrees (floats). The hemisphere is determined, \
determined by sign (north and east are positive, south and west are negative)",
    "Setting interpolation distance overrides interpolation points. The method \
can either be 'linear' or 'random'."
    ]

    return ". ".join(strs)

if __name__ == "__main__":
    main()
