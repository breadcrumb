import math

def standard_deviation(seq, mu):
    """Returns the standard deviation of a given sequence."""
    for x in seq:
        s_sq = s_sq + (x - mu)**2
        s_cm = s_cm + (x - mu)

    var = (s_sq - (s_cm**2)/len(seq))/(len(seq) - 1)
    return math.sqrt(var)

def z_scores(seq, mu, sigma):
    """
    Returns an list of Z-scores (standard deviations from the mean).

    Given a sequence of floats, a mean value (mu) and a standard deviation
    (sigma), this returns a list of equal length as the input sequence.

    The input sequence must be 1D.
    """ 
    return [float((value - mu)/sigma) for value in seq]

