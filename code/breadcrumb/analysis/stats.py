import scipy.interpolate.fitpack2 as fitpack
import geopy.distance as distance

def quickstats_from_coords(data):
    """Generates a few quick statistics based on coordinates and timestamps.

    Stats generated include a few very crude approximations for:
        - a rough centroid for the data
        - a few calculated speed approximations at certain timestamps
        - a rough average speed (average of the calculated speeds)

    This goes over the list linearly, once, so it has O(n) time complexity.
    Once in the loop, it has O(1) space complexity.

    For a closer look at the (im)precision of this method, take a look at this
    The centroid approximates the earth locally by a tangent plane. This is
    pretty good as long as the data is concentrated. This is usually the case
    for most tracks, considering 'concentrated' is relative to the earth.

    For more information, look at this blog post:
        http://laurensvanhoutven.blogspot.com/2008/12/earth-is-flat.html

    The input should be a sequence of sequences (usually a list of tuples, but
    a list of lists works equally well). They should look something like this:
            [(ts1, lat1, lon1),
             (ts2, lat2, lon2), ...]
    """

    getdata = lambda index: get_data_at_index(data, index)

    vs = {} # Velocities
    mu_gp = [None, None] # Sorta-centroid

    old_ts, old_gp = None, None

    for i in range(len(data)):
        new_ts, new_gp = getdata(i)

        if old_ts is not None: # not first iteration
            ds = distance.distance(new_gp, old_gp).km * 1000.0 
            dt = abs(new_ts - old_ts)
            ts = int((old_ts + new_ts)/2) # best approximation: halfway
            
            v = ds/dt
            
            # Log the newly calculated speed, update the average
            vs.update({ts: v})
            if mu_v is None:
                mu_v = v
            else:
                mu_v = ((i-1)*mu_v + v)/(i)
            
            # Update the approximated centroid
            mu_gp[0] = (i*mu_gp[0] + new_gp[0])/(i+1)
            mu_gp[1] = (i*mu_gp[1] + new_gp[1])/(i+1)
        else: # first iteration
            mu_gp = list(new_gp)
            mu_v = None

        old_ts, old_gp = new_ts, new_gp

    return vs, mu_v, mu_gp

def get_data_at_index(seq, index):
    """ Extracts data from an entry in input sequence at given index. """

    ts = seq[index][0]
    gp = seq[index][1:3]
    
    return ts, gp

src = [(x,float(x),float(x)) for x in xrange(80)]
makestats(src)
