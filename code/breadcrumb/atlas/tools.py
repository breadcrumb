# -*- coding: utf8 -*-
""" Generic tools useful for mapping coordinates to other values. """

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import operator

def getbounds(sequence, lat_index = 0, lon_index = 0):
    """
    Returns the bounds for a given sequence of sequences.
    
    By default, assumes input looks something like this:
    [(lat0, lon0), (lat1, lon1), (lat2, lon2), ...]
    
    Returns a tuple filled with the minimums of latitude and longitude followed
    by the maximums of latitude and longitude.
    """
    # XXX: See if this function takes a lot of time. If it does, rewrite so it
    # only goes over the sequence once (but in Python instead of C), profile it
    # again, and see which one is faster. Also, try Psyco.
    
    # XXX: Once this is done, remove one of the implementations!

    #return _getbounds_minmax(sequence, lat_index, lon_index)
    return _getbounds_linsearch(sequence, lat_index, lon_index)

def _getbounds_minmax(sequence, lat_index, lon_index):
    """ Implements getbounds() with Python's min() and max() builtins. """
    lat, lon = operator.itemgetter(lat_index), operator.itemgetter(lon_index)
    
    minimum = lambda key: min(sequence, key = key)
    maximum = lambda key: max(sequence, key = key)

    return minimum(lat), minimum(lon), maximum(lat), maximum(lon)

def _getbounds_linsearch(sequence, lat_index, lon_index):
    """ Implements getbounds() through linear search. """

    minlat, minlon, maxlat, maxlon = 0, 0, 0, 0

    for tup in sequence:
        if tup[lat_index] > maxlat:
            maxlat = tup[lat_index]
        elif tup[lat_index] < minlat:
            minlat = tup[lat_index]
        
        if tup[lon_index] > maxlon:
            maxlon = tup[lon_index]
        elif tup[lon_index] < minlon:
            minlon = tup[lon_index]

    return minlat, minlon, maxlat, maxlon
