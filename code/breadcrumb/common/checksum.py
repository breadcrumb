# -*- coding: utf8 -*-
""" Functions for creating and validating NMEA style checksums. """

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

def is_valid_nmea_sentence(sentence):
    """Validates the checksum of an NMEA sentence.
    
    Returns True if the sentence has either a valid checksum or no checkum. 
    Returns False if the checksum was bad.
    """
    if sentence[-3] == '*': # sentence has a checksum
        reference, source = sentence[-2:], sentence[1:-3]
        return is_valid_hex_csum(source, reference)
    else: # sentence does not have a checksum
        return True
    
def is_valid_hex_csum(string, reference):
    """Calculates a hex checksum and compares it to the reference."""
    return generate_hex_checksum(string) == reference

def generate_checksum(string):
    """Generates an NMEA style checksum (in decimal) of a given string."""
    checksum = 0
    for char in string:
        checksum = checksum ^ ord(char)
    return checksum

def generate_hex_checksum(string):
    """Generates the hexadecimal form of the checksum of a given string."""
    hex_csum = "%02x" % generate_checksum(string)
    return hex_csum.upper()

def generate_byte_checksum(string):
    """Generates the byte form of the checksum of a given string."""
    return chr(generate_checksum(string))
