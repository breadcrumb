# -*- coding: utf8 -*-
""" Common Breadcrumb constants. """ 

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

VERSION = file(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                            'VERSION'), 'r').read()

DEFAULT_PORTS = {
    'TCP': 27861,
    'UDP': 27862,
    'SSH': 27863,
}

KNOWN_ENCAPS = DEFAULT_PORTS.keys()

CRUMB_FIELDS = [
    ('timestamp',   ('timestamp',),              'L'  ),
    ('coordinates', ('latitude', 'longitude'),   'ff' ),
    ('velocity',    ('velocity',),               'f'  ),
    ('heading',     ('heading',),                'f'  ),
    ('handlermsg',  ('handlerid', 'handlermsg'), 'c8s'),
]

SERVER_CONFIG_FILENAMES = [
    'server.ini',
    '~/.breadcrumb/server.ini',
]
