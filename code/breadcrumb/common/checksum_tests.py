# -*- coding: utf8 -*-
""" Tests for checksum.py. """

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import checksum

class TestChecksumWrappers(unittest.TestCase):
    def setUp(self):
        pass

    def test_is_valid_nmea_sentence(self):
        """ Tests for the NMEA sentence checker. """
        # Good checksum:
        self.assertTrue(checksum.is_valid_nmea_sentence('$GPGGA,123519,4807.03\
        8,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47'), 'NMEA sentence, valid \
        checksum, should pass')
    
        # No checksum:
        self.assertTrue(checksum.is_valid_nmea_sentence('$GPGGA,123519,4807.03\
        8,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,'), 'NMEA sentence, no check\
        sum, should pass')
    
        # Bad checksum:
        self.assertFalse(checksum.is_valid_nmea_sentence('$GPGGA,123519,4807.0\
        38,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*53'), 'NMEA sentence, bad \
        checksum, should not pass')

if __name__ == '__main__':
    unittest.main()
