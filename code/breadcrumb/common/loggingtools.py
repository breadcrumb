# -*- coding: utf8 -*-
"""Common logging tools."""

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import re

def get_logging_level(name = None):
    """Maps a wide range of verbosity names to logging levels."""
    if name is None:
        return VERBOSITIES['default']
     
    name = re.sub(r'[^a-z]', '', name.lower());

    if name not in VERBOSITIES:
        return VERBOSITIES['default']
    else:
        return VERBOSITIES[name]

VERBOSITIES = {
    'veryquiet': logging.CRITICAL,
    'silent': logging.CRITICAL,
    'critical': logging.CRITICAL,

    'quiet': logging.ERROR,
    'low': logging.ERROR,
    'error': logging.ERROR,
    
    'default': logging.INFO,
    'normal': logging.INFO,
    'standard': logging.INFO,
    'info': logging.INFO,

    'loud': logging.DEBUG,
    'high': logging.DEBUG,
    'verbose': logging.DEBUG,
    'debug': logging.DEBUG,
}
