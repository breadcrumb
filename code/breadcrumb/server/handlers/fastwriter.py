# -*- coding: utf8 -*-
"""A handler for creating KML/GPX files really quickly (on-line)."""

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from time import strftime
from datetime import datetime

import base

class FastFileWriter(base.BaseHandler):
    """A handler for generating XML track files (KML, GPX) on-line.

    This handler works on a flat file principle. Every new point appends a
    small bit of text to a flat file. This means that the XML tree is never
    complete (it's always missing the closing tags) during operation. The end
    tags are written to the file as soon as the handler closes down. This
    behaviour can be changed with the ``finalize`` parameter (see below).
    
    Another consequence is that it's impossible to change any of the headers or
    previously added elements of the file while the handler is running. The
    file must be finalized and then parsed with any XML parser, such as lxml.
        
    The advantages are simplicity and speed. Because turning a flat file into
    an XML file is simply appending a small amount of text, it can usually be
    done in constant time. Generating a track from a database means executing
    SQL queries and generating XML, which would take a lot longer.
    
    This module provides minimal (read: no) protection against race conditions.

    Several *file formats* are available. 
    
    Most file formats have several *modes*. In KML, you can write down points
    as part of a path, or as separate markers, for example. If unset, uses the
    default mode for the current file format.

    :parameter: ``filename`` (string)
                
        The filename to open. See the open_file method.
    
    :parameter: ``format`` (string)
                
        The name of the file format. Can be one of 'kml' or 'gpx'.
    
    :parameter: ``mode`` (string)
                
        The specific mode of a file format.
    
    :parameter: ``track_name`` (string)
                
        The name of the track. Used in some templates.
    
    :parameter: ``track_description`` (string)
                
        The description of the track. Used in some templates.
    
    :parameter: ``finalize`` (boolean)
                
        If True, finalizes the file when the handler shuts down.

    :parameter: ``**kwargs``
                
        The keyword arguments are passed to the open_file method and the
        initializer of the superclass. They are also used to interpolate
        template strings.
    """
    def __init__(self,
                 filename = None, format = "kml", mode = None,
                 track_name = "Track", track_description = "Breadcrumb track",
                 finalize = True, **kwargs):
        base.BaseHandler.__init__(self,
                                  identifiers = ('\x20', '\x21'), **kwargs)
        
        self.mode = mode or TEMPLATES[format]['modes']['default']
        
        self.format = format
        
        self.header, self.template, self.footer = self._generate_templates(
            track_name = track_name,
            track_description = track_description,
            **kwargs)

        self.open_file(filename = filename, **kwargs)
        self.finalize = finalize

        self.messagehandlers.extend([
            {
                'regex': '\x00*',
                'method': self.open_file,
            },
            {
                'regex': '\x01*',
                'method': self.close_file,
            },
            {
                'regex': '\x02*',
                'method': self.cycle_file,
            },
        ])

    def _generate_templates(self, **kwargs):
        """Generate the template attributes.
        
        :parameter: ``**kwargs`` (strings)
                    
            Keyword arguments are used as an interpolation dictionary for the
            templates.
        """
        type_templates = TEMPLATES[self.format]
        mode_templates = type_templates['modes'][self.mode]

        header = type_templates['header'] + mode_templates['header']
        header = header % kwargs
        template = mode_templates['template']
        footer = mode_templates['footer'] + type_templates['footer']

        return header, template, footer

    def newpoint(self, point):
        """Appends a new point to the current file."""
        if self.filehandle is None:
            self.logger.error("Caught point, but no file was open")
            return
        
        point['timestamp'] = \
            datetime.utcfromtimestamp(float(point['timestamp'])).ctime()
        outstr = self.template % point
        self.filehandle.write(outstr)

    def clean_up(self):
        """Closes the file, finalizing the XML tree.
        
        :effect: ``close_file()``
        """
        self.close_file()

    def open_file(self,
                  filename = None, initialize = True, force = False, **kwargs):
        """Opens a new file for writing.
        
        :parameter: ``filename`` (string)
        
            The file name to open. If unspecified, picks a filename based on
            the current time and date.

        :parameter: ``initialize`` (string)
            Initializes the target file.
        
            This causes the handler to write the header for the current file
            format before writing anything else. This is a good idea for new
            files. If you want to append points to an existing, unfinalized
            file, set this to a False value.

        :parameter: ``force`` (boolean)
            Initializes the target file, even if it isn't empty.
        """
        filename = filename \
                 or strftime("track-%d%b%Y-%H%M.fast%(type)s")\
                             % {'type': self.format}

        self.filehandle = open(filename, 'a')

        if initialize:
            if not is_empty(self.filehandle):
                if not force:
                    raise RuntimeError, "File already exists and not empty."
                else:
                    self.logger.info("File isn't empty, but writing headers "
                    "anyway because force is a True value (%s)..." % True)

            self.filehandle.write(self.header)

    def close_file(self):
        """Closes the current file, closing the XML tree.
        
        This should turn the fast[kml|gpx] file into a *real* KML/GPX file,
        suitable for importing in other programs (such as Google Earth).

        This will disable writing to the file first, so no more writes happen
        during the short amount of time that we're closing the file. This isn't
        guaranteed to be 100% safe, but it seems to work pretty well.

        New points caught by this handler will be discarded after this method
        is called, until a new file is opened.
        """
        filehandle = self.filehandle
        self.filehandle = None # Prevent accidental writing during closing
        
        if self.finalize:
            filehandle.write(self.footer)
        
        filehandle.close()

    def cycle_file(self):
        """Closes the currently open file and opens a new one.

        The new file will have the default filename.

        This is a shorthand used for handler messages.
        """
        self.close_file()
        self.open_file()

def is_empty(file):
    """Returns True if a file is empty, False otherwise."""
    return os.fstat(file.fileno()).st_size == 0

KML_HEADER = """<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
<Document>
<name>%(track_name)s</name>
<description>%(track_description)s</description>
"""
KML_PATH_HEADER = """<Style id="yellowLineGreenPoly">
<LineStyle><color>7f00ffff</color><width>4</width></LineStyle>
<PolyStyle><color>7f00ff00</color></PolyStyle>
</Style>
<Placemark><styleUrl>#yellowLineGreenPoly</styleUrl>
<LineString>
<extrude>1</extrude><tessellate>1</tessellate><altitudeMode>absolute</altitudeMode>
<coordinates>
"""
KML_PLACEMARK_TEMPLATE = """<Placemark><name>Breadcrumb tracking frame</name>
<description>%(timestamp)s</description>
<Point><coordinates>%(longitude)s,%(latitude)s,0</coordinates></Point>
</Placemark>
"""

GPX_HEADER = """<?xml version="1.0"?>
<gpx
version="1.1"
creator="Breadcrumb"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns="http://www.topografix.com/GPX/1/1"
xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
"""
GPX_ROUTE_TEMPLATE = """<rtept lat="%(latitude)s" lon="%(longitude)s">
<time>%(timestamp)s</time>
<sym>Dot</sym>
</rtept>
"""
GPX_WAYPOINT_TEMPLATE = """<wpt lat="%(latitude)s" lon="%(longitude)s">
<time>%(timestamp)s</time>
<sym>Dot</sym>
</rtept>
"""

TEMPLATES = {
    'kml': {
        'header': KML_HEADER,
        'modes': {
            'default': 'path',
            'path': {
                'header': KML_PATH_HEADER,
                'template': "%(longitude)s,%(latitude)s,100\n",
                'footer': "</coordinates></LineString></Placemark>",
            },
            'placemarks':{
                'header': '',
                'template': KML_PLACEMARK_TEMPLATE,
                'footer': '',
            },
        },
        'footer': "</Document></kml>",
    },
    'gpx': {
        'header': GPX_HEADER,
        'modes': {
            'default': 'route',
            'route': {
                'header': '<rte>',
                'template': GPX_ROUTE_TEMPLATE,
                'footer': '</rte>',
            },
            'waypoints': {
                'header': '',
                'template': GPX_WAYPOINT_TEMPLATE,
                'footer': '',
            },
        },
        'footer': "</gpx>",
    }
}

