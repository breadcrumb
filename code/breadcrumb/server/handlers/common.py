# -*- coding: utf8 -*-
"""Common handler behaviour."""

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import collections
import logging

from base import BaseHandler

class PointQueueHandler(BaseHandler):
    """An abstract handler for handlers that have point queues.

    Handlers that sublcass this should not implement a newpoint method.

    This assumes the method implements a render() method, which takes at least
    an iterable of points and returns a string or unicode object, and a send()
    method that takes that string or unicode object and sends it.
    """
    def __init__(self, burst_length = 1, **kwargs):
        BaseHandler.__init__(self, identifiers = ('\x03'), **kwargs)

        self.queue = collections.deque()
        self.burst_length = burst_length

    def newpoint(self, point):
        """Adds a point to the queue.
        
        This also checks if the queue is full. If it is, we clear it and handle
        it appropriately (render to a string and send the string).
        """
        self.queue.append(point)
        
        if len(self.queue) > self.burst_length:
            outstr = self.render(points = self.queue())
            self.send(outstr)
    
    def render(self, points):
        """Renders the points into a unicode or string object.
        
        Concrete classes should implement this method."""
        raise NotImplementedError

    def send(self, outstr):
        """Sends the given output string.
        
        Concrete classes should implement this method."""
        raise NotImplementedError

class Renderer(object):
    """Mixin that provides rendering for handlers.

    By default, this tries to use Mako templating.

    If the requested templating method or the default method is unavailible,
    the initializer will complain loudly and revert a simple renderer which
    uses string interpolation.

    Classes that inherit from this get a render method. That method is
    guaranteed to accept at least a keyword argument named points, which
    should be an iterable of point dictionaries. This method returns the
    string representation of those points, rendered by whatever rendering
    engine was used.
    """
    def __init__(self, type = None):
        self.render = self._get_render_method(type)
    
    def _get_render_method(renderer = None):
        """Gets a method that render points.
        
        If no type is specified, it tries Mako, but falls back to SimpleRenderer
        if Mako is not availible. The same thing happens if you manually specify
        Mako but it is unavailible.
        """
        if renderer is None or renderer.lower() == 'mako':
            try:
                renderer = MakoTemplateRenderer()
                logging.debug("using mako renderer...")
            except ImportError:
                logging.error("can't import mako, using simple templating...")
                renderer = SimpleTemplateRenderer()
        elif renderer.lower() == 'simple':
            renderer = SimpleTemplateRenderer()
            logging.debug("using simple renderer...")
        else:
            raise RuntimeError, "unknown template renderer type %s" % type

        return renderer.render

class SimpleTemplateRenderer(object):
    """Implements simple (string interpolation) templating.

    *Please* use Mako templating."""
    def __init__(self,
                 templatedirs = './',
                 templatefile = None,
                 templatevar  = None):
        logging.debug("Using the simple template renderer...")
        if templatevar is None:
            logging.debug("No template var given...")
            if templatefile is None:
                logging.debug("No template file given.")
                logging.debug("Using default template...")
                self.template = DEFAULT_SIMPLE_TEMPLATE
            else:
                for templatedir in templatedirs:
                    try:
                        filename = os.path.join(templatedir, templatefile)
                        logging.debug("Trying template file %s..."% filename)
                        filehandle = open(filename, 'r')
                    except IOError:
                        logging.debug("Didn't work, trying next one...")
                        continue
                self.template = filehandle.read()
        else:
            logging.debug("Using provided template variable...")
            self.template = templatevar

    def render(self, points = None, **kwargs):
        """Renders a set of points using string interpolation.
        
        Interpolation happens with a default value if the requested value
        is not present in the point.
        """
        if not points:
            logging.error("no points to render :-(")
            return
        strings = []
        for point in points:
            interpolationdict = collections.defaultdict(lambda: 'N/A')
            interpolationdict.update(point)
            interpolationdict.update(kwargs)
            strings.append(self.template % interpolationdict)
        return ''.join(strings)

DEFAULT_SIMPLE_TEMPLATE = """
ts: %(timestamp)s, lat/lon: %(latitude)s, %(longitude)s
"""

class MakoTemplateRenderer(object):
    """Implements Mako templating."""
    def __init__(self,
                 templatedirs = './',
                 templatefile = None,
                 templatevar  = None):
        
        from mako.template import Template
        from mako.lookup import TemplateLookup

        if templatevar is not None:
            self.template = Template(templatevar)
        else:
            templatedirs = filter(None, templatedirs.split(','))
            self.lookup = TemplateLookup(list(templatedirs))
        
            if templatefile is not None:
                self.template = self.lookup.get_template(templatefile)
            else:
                logging.info("No template given, reverting to default...")
                self.template = Template(DEFAULT_MAKO_TEMPLATE)

    def render(self, **kwargs):
        """Renders the Mako template."""
        return self.template.render(**kwargs)

DEFAULT_MAKO_TEMPLATE = """
% for point in points:
% for key, value in point.iteritems():
${key}: ${value}
% endfor
\n
% endfor
"""
