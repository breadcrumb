# -*- coding: utf8 -*-
"""SQLAlchemy based database handler."""

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time
import logging

import sqlalchemy
from sqlalchemy import Table, Column, ForeignKey
from sqlalchemy.types import Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relation, backref

from base import BaseHandler

class SQLAlchemyHandler(BaseHandler):
    """Writes points to a database using SQLAlchemy."""
    def __init__(self, dburi = 'sqlite:///:memory:', dblogging = False):
        BaseHandler.__init__(self, identifiers = ('\x10', '\x11'))
        
        self.connect(dburi, dblogging)
        Base.metadata.create_all(self.engine)

    def connect(self, dburi, dblogging):
        """Connects to the database."""
        self.engine = sqlalchemy.create_engine(dburi, echo = dblogging)
        self.Session = sessionmaker(bind = self.engine)

    def newpoint(self, point):
        """Writes a new point to the database."""
        session = self.Session()
        dbpoint = Point(point)
        session.add(dbpoint)
        session.commit()

Base = declarative_base()

class Track(Base):
    """A collection of points."""
    __tablename__ = 'tracks'
    id = Column(Integer, primary_key = True)
    name = Column(String)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "<Track('%s')>" % self.name

pointattrs = [
    'timestamp',
    'latitude',
    'longitude',
    'altitude',
    'heading',
    'velocity'
]

class Point(Base):
    """A point (decoded crumb)."""
    __tablename__ = 'points'
    id = Column(Integer, primary_key = True)
    
    trackid = Column(Integer, ForeignKey('tracks.id'))
    track = relation(Track, backref=backref('points', order_by='timestamp'))

    timestamp = Column(Integer)
    latitude  = Column(Float)
    longitude = Column(Float)
    altitude  = Column(Float)
    heading   = Column(Float)
    velocity  = Column(Float)

    def __init__(self, point):
        """Creates a Point from a dict of point data."""
        for attr in pointattrs:
            if attr in point:
                setattr(self, attr, point[attr])

