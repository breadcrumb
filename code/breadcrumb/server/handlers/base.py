# -*- coding: utf8 -*-
"""Base handlers for the Breadcrumb server."""

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import time
import re

from breadcrumb.common.loggingtools import get_logging_level

class BaseHandler(object):
    """The handler superclass. All other handlers should inherit from this."""
    def __init__(self,
                 name = None,
                 identifiers = None,
                 verbosity = 'normal',
                 **kwargs):
        self.identifiers = set(['\x00'])
        if identifiers:
            self.identifiers.add(identifiers)

        self.messagehandlers = []

        # identifiers is a set because:
        #     - uniqueness makes sense
        #     - order doesnt matter
        #     - all operations on it are membership checks

        # messagehandlers is a list because:
        #     - uniqueness would make sense, but rarely matters
        #     - order might be important
        #     - operations are linear search anyway (find matching regex)

        if name is None:
            # If you don't specify a handler instance name specifically, your
            # punishment is having a terribly long and useless name. Enjoy!
            name = 'Breadcrumb.Handlers.%s.%s' % (type(self).__name__, id(self))
        else:
            name = 'Breadcrumb.Handlers.%s' % name

        self.logger = logging.getLogger(name)
        self.logger.setLevel(get_logging_level(verbosity));

        self.logger.info("Created a new %s ..." % type(self).__name__)

    def newpoint(self, point):
        """Callback called whenever a new data point arrives.
        
        This is usually overwritten by subclasses.
        """
        raise NotImplementedError

    def newmessage(self, identifier, content):
        """Does something with a handler message.

        This is a generic method that works as long as the handler defines
        two attributes: ``identifiers`` and ``messagehandlers``
            
        The latter is a list of dicts. The dicts contain a regex that matches
        something, and a method (can be any callable). 
        
        If the message contains arguments that should be passed to the method,
        the regex should use named captures: the method is then called with
        ``**matches`` as argument. If the regex has no named captures, the
        method is called with no arguments.
        """
        
        if not identifier in self.identifiers:
            return 

        for handler in self.messagehandlers:
            if 'compiledregex' not in handler:
                # First use, so compile the regex
                self.logger.debug('compiling regex %s...' % handler['regex'])
                handler['compiledregex'] = re.compile(handler['regex'])

            match = handler['compiledregex'].match(content)

            if match is None:
                self.logger.info("regex %s didn't match content %s..." %
                    (handler['regex'], content))
                return
            
            # match is not None:
            handler['method'](**(match.groupdict() or {}))

    def clean_up(self):
        """Callback called whenever the server closes down.

        This is sometimes overwritten by subclasses.
        """
        raise NotImplementedError

class EchoHandler(BaseHandler):
    """A handler that logs new data points.
    
    By default, this uses the logging module, which should automatically
    do the right thing. If you don't want to use the logging module, set
    the use_logging flag to a false value. The handler will then revert to
    using print statements instead.
    """
    def __init__(self, use_logging = True, **kwargs):
        BaseHandler.__init__(self, identifiers = ('\x02'), **kwargs)
        
        self.use_logging = use_logging

    def newpoint(self, point):
        """Logs a data point."""
   
        if 'timestamp' in point:
            point['timestamp'] = time.ctime(point['timestamp'])

        message = "Caught point! Data: %s" % repr(point)

        if self.use_logging:
            self.logger.info(message)
        else:
            print message

class SequentialHandler(BaseHandler):
    """A handler that sequentially passes events to other handlers.
    
    A server instance, by itself, only supports a single handler. If you want
    to use multiple handlers, pass an instance of this class to the server
    instead, and add any other handler to the sequential handler using the
    ``add_handler()`` method.

    When the server calls one of its callbacks, the sequential handler calls
    that callback, with the same arguments, on all of its child handlers. This
    sequence has a consistent, but unpredictable order. 

    This also does exception handling in case one of the child handlers doesn't
    implement a certain callback.
    """
    def __init__(self, **kwargs):
        BaseHandler.__init__(self, identifiers = ('\x01'), **kwargs)
        self.handlers = set()

    def add_handler(self, handler):
        """Adds a new handler to the sequence of handlers.
        
        :postcondition: 
            This method refuses to add handlers unless they inherit from
            BaseHandler. This is because it relies on specific exceptions
            being thrown for unimplemented methods.
            ::

                if not isinstance(handler, BaseHandler):
                    raise RuntimeError

        :postcondition:
            If the argument is a sequential handler, its handlers are added to
            our own.
            ::
                
                if isinstance(handler, SequentialHandler):
                    self.handlers.update(handler.handlers)

            If you try to add a sequential handler to itself, this
            short-circuits. Since children live in a set, this has the same
            effect as the above (albeit faster).
            ::
                
                if handler is self:
                    return

        :postcondition:
            If the argument is any other handler, it's added to the set of
            handlers.
            ::
                
                self.handlers.add(handler)
        """
        if handler is self:
            self.logger.error("add_handler failed, got myself as argument")
            return

        if isinstance(handler, SequentialHandler):
            self.handlers.update(handler.handlers)
        elif isinstance(handler, BaseHandler):
            self.handlers.add(handler)
        else:
            raise RuntimeError, 'Suggested handler not subclass of BaseHandler'
    
    def call_children(self, method_name,
                      args_callable = lambda: [],
                      kwargs_callable = lambda: {},):
        """Calls a given method on each of the child handlers.
        
        This catches the exceptions that child handlers throw, and continues on
        to the next handler, until all of them have been called.

        :parameter: ``method_name`` (string)

            The name of the method to call.

        :parameter: ``args_callable`` (callable, returns list)
            
            A callable that is evaluated to produce the list which is then used
            as the positional arguments (``*args``) in the child method calls.

        :parameter: ``kwargs_callable`` (callable, returns dict)
            
            A callable that is evaluated to produce the dict which is then used
            as the keyword arguments (``**kwargs``) in the child method calls.
        """
        for handler in self.handlers:
            try:
                method = getattr(handler, method_name)
                logging.debug("Got method %s..." % method)
            except NameError:
                errstr = "handler %s doesn't *have* a %s method..."
                logging.debug(errstr % (repr(handler), method_name))
                continue

            try:
                method(*args_callable(), **kwargs_callable())
            except NotImplementedError:
                errstr = "handler %s doesn't implement %s method..."
                logging.debug(errstr % (repr(handler), methodname))
                continue

    def newpoint(self, point):
        """Passes the given point to all of the registered handlers.
        
        This copies the point, so that the effects of one handler don't affect
        any of the ones that follow it. 

        :parameter: ``point`` (dict)

            The original point. A copy of this point is passed to all
            registered handlers.
        """
        self.call_children(method_name = 'newpoint',
                           args_callable = lambda: [point.copy()])

    def newmessage(self, id, message):
        """Passes the given message to all of the registered handlers."""
        self.call_children(method_name = 'newmessage',
                           args_callable = lambda: [id, message])

    def clean_up(self):
        """Tells the child handlers to clean up."""
        self.call_children(method_name = 'clean_up')
