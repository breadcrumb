"""
Handlers are objects that are called when the server catches and decodes a crumb. Hanlders are the things that actually do all of the heavy lifting in the server process. They inherit from :ref:`BaseHandler`.
"""
