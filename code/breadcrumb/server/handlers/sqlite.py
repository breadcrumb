# -*- coding: utf8 -*-
""" Database handlers for the Breadcrumb server. """

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sqlite3
import logging

import base

class SQLiteHandler(base.BaseHandler):
    """A handler that writes new data points to a SQLite database.
    
    This uses the ``sqlite3`` module, which is a builtin on recent versions of
    Python. It (obviously) still requires you to install SQLite itself.

    This handler is not entirely deprecated, but using the SQLAlchemy-based
    handler is strongly preferred.

    :parameter: ``filename`` (string)

        The filename of the SQLite database.

    :parameter: ``table`` (string)
        
        The name of the table in the SQLite database.

    :parameter: ``initialize`` (boolean)
        
        If this evaluates to True (default), creates the database table. Useful
        for new files.
    """

    def __init__(self,
                 filename = "breadcrumb.db",
                 table = 'breadcrumb', initialize = True,
                 **kwargs):
        BaseHandler.__init__(self, identifiers = ('\x10', '\x12'), **kwargs)
                
        self.connection = sqlite3.connect(filename)
        self.cursor = self.connection.cursor()
        
        self.table = tablename

        if initialize:
            create_table()

    def create_table(self):
        """Prepares the database for incoming points by creating the table."""
        
        sql = "create table %s (time int, lat real, lon real)" % self.table
        self._execute(sql)

    def newpoint(self, point):
        """Adds a point to the database.
        
        :parameter: ``point`` (dict)

            The point that will be added to the database.
        """
        
        sql = "insert into %s values ('%s', '%s', '%s')"\
            % (self.table, timestamp, latitude, longitude)
        self._execute(sql)

    def _execute(self, sql):
        """Executes a given SQL string."""
        logging.debug("sqlitehandler, executing: %s" % sql)
        self.cursor.execute(sql)
        self.connection.commit()
