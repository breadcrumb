# -*- coding: utf8 -*-
""" HTTP handler code for Breadcrumb servers. """

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import urllib
import httplib
import logging
import time

from common import PointQueueHandler, Renderer

class HTTPHandler(PointQueueHandler, Renderer):
    """A handler for sending data over HTTP.
    
    ``action`` and ``name`` mean the same thing as in an HTML form. This is
    illustrated by the following example:
    
    To POST something to a server for which the URL looks like this::
    
        http://test.example:8080/my_cgi_app
    
    ... and the element looks something like this:: 
        
        <input name="foo" action="/my_cgi_app" method='POST' />

    you would create a handler instance like this::
    
        HTTPHandler(host = "text.example",
                    port = 8080,
                    action = "/my_cgi_app",
                    name = "foo")
        
    We don't need to pass the method here, because POST is the default. You can
    use any string httplib understands (``GET``, for example.).
    
    This handler opens a new HTTP connection for each submission.
    """
    def __init__(self,
                 host = "localhost", port = 80,
                 action = None, name = None, method = 'POST',
                 burst_length = 1,
                 rendertype = None):
        PointQueueHandler.__init__(self,
                                   burst_length = burst_length,
                                   identifiers = ('\x30', '\x31'))
        Renderer.__init__(self, type = rendertype)

        if not all([action, name, method]):
            raise RuntimeError, 'not enough parameters for HTTPHandler'
        else:
            self.action, self.element, self.method = action, name, method
        
        self.host, self.port = str(host), int(port)
        self.headers = {}
   
        for identifier in ['\x30, \x31']:
            self.identifiers.add(identifier)

        logging.debug("Initialized HTTPHandler at %s:%s" % (host, port))

    def send(self, outstr):
        """Sends the given string across HTTP."""
        parameters = urllib.urlencode({self.element: outstr})
        
        connection = httplib.HTTPConnection("%s:%d" % (self.host, self.port))
        connection.request(self.method, self.action, parameters, self.headers)
        connection.close()
