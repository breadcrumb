# -*- coding: utf8 -*-
"""SMTP reporting for the Breadcrumb server."""

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import email.utils
import email.mime.text
import smtplib
import logging

from common import PointQueueHandler, Renderer

class SMTPHandler(PointQueueHandler, Renderer):
    """Sends mail digests of a number of points."""
    def __init__(self, source, destinations,
                 subject = "Breadcrumb tracking report",
                 server = 'localhost', port = 25,
                 threshold = 10, rendertype = None,
                 usetls = True):
        PointQueueHandler.__init__(self,
            threshold = threshold,
            identifiers = ('\x30', '\x32'))
        Renderer.__init__(self, type = rendertype)

        self.source = source
        self.destinations = destinations.split(',')

        self.usetls, self.threshold = usetls, threshold

        logging.debug("Initialized SMTPHandler for %s:%s, from: %s, to: %s" \
        % (host, port, source, destinations))

    def send(self, out_string):
        """Sends the given sring using SMTP."""
        mime_string = self.mime_encode(out_string)
        self._raw_send(mime_string)

    def mime_encode(self, message):
        """Encodes a given message in MIME format."""
        mime = email.mime.text.MIMEText(message)
        
        mime.set_unixfrom('author')
        destinations = ', '.join(self.destinations)
        mime['To'] = email.utils.formataddr(('Recipient', destinations))
        mime['From'] = email.utils.formataddr(('Author', self.source))
        mime['Subject'] = self.subject
        
        logging.debug("Email message mime encoding finished...")

        return mime.as_string()

    def _raw_send(self, mimestring):
        """Passes the MIME string to the SMTP server for sending."""
        server = smtplib.SMTP("%s:%s" % (self.hostname, self.port))
        server.connect()
        try:
            server.set_debuglevel(True)
            server.ehlo()

            if self.usetls and server.has_extn('STARTTLS'):
                logging.debug('SMTP server has TLS support, starting TLS...')
                server.starttls()
                server.ehlo()
            
            if self.username is not None:
                logging.debug("Sending SMTP credentials...")
                server.login(self.username, self.password)
            
            server.sendmail(self.source, self.destinations, message)
            server.close()
        except:
            logging.error("Message sending failed!")
