# -*- coding: utf8 -*-
"""Tk GUI handler for the Breadcrumb server.

Do not load this handler without activating Twisted's tksupport!
Twisted and Tk both have an event loop, and not telling Twisted about Tk in the
background will get you a functioning server, but a dead UI.

Look at the scripts that start the GUI for an example on how to do this.
"""

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from Tkinter import Button, Entry, Frame, Label, StringVar, Text
from Tkinter import CURRENT, DISABLED, NORMAL, LEFT, RIGHT
from twisted.internet import reactor
from time import strftime

from base import BaseHandler

class TkGUIHandler(BaseHandler):
    """A Tk-based GUI to run the Breadcrumb server.
    
    This handler is special in the sense that it has a reference to its own
    server. This is required, because we need to be able to control it.
    """
    def __init__(self, master):
        BaseHandler.__init__(self)
        
        self.main_frame = Frame(master)
        self.main_frame.pack()

        self.server  = None
        self.port    = None

        self.buttons = {}
        
        control_frame = Frame(self.main_frame).pack(side = LEFT)
        output_frame  = Frame(self.main_frame).pack(side = LEFT)
        
        self._make_udp_netconfig_widgets(control_frame)
        self._make_status_widgets(control_frame)
        self._make_control_widgets(control_frame)
    
        self._make_output_frame(output_frame)
    
    def newpoint(self, point):
        """ Logs a data point to the output window. """
        log_string = "New point. ts: %s, lat: %+3f, lon: %+3f"\
            % (point['timestamp'], point['latitude'], point['longitude'])
            
        self.write(log_string)

    def _make_control_widgets(self, parent):
        """ Creates the main control buttons. """
        button_frame = Frame(parent)
        button_frame.pack()
        
        buttons_data = [
            ['Start server', self.start, 'start'],
            ['Stop server',  self.stop,  'stop' ],
        ]
        
        for button_data in buttons_data:
            [text, command, short] = button_data
            
            self.buttons[short] = Button(button_frame,
                                         text = text,
                                         command = command)
            self.buttons[short].pack(side = LEFT)
    
        self.buttons['stop']['state'] = DISABLED
    
    def _make_status_widgets(self, parent):
        """ Creates a frame that displays the status of the server. """
        frame = Frame(parent)
        frame.pack()
        
        self.status_string = StringVar()
        self.status_string.set("stopped")
        
        Label(frame, text = "Server status:").pack(side = LEFT)
        Label(frame, textvariable = self.status_string).pack(side = RIGHT)
    
    def _make_udp_netconfig_widgets(self, parent):
        """ Creates a frame for configuring UDP server parameters. """ 
        frame = Frame(parent)
        frame.pack()
        
        # Title label:
        Label(frame, text = "Server configuration").grid(columnspan = 2)
        
        Label(frame, text = "Port").grid(row = 1)
        self.port_entry = Entry(frame)
        self.port_entry.grid(row = 1, column = 1)
    
    def _make_output_frame(self, parent):
        """ The frame for displaying server output. """
        Label(parent, text = "Output:").pack()
        
        self.output = Text(parent, width = 100)
        self.output.tag_config("ERROR", foreground = "red")
        self.output.pack()
    
    def write(self, message, tag = "a"):
        """
        Writes the message to the output text box.
        """
        timestamp = strftime('%02H:%02M:%02S')        
        self.output.insert(CURRENT, "%s | %s\n" % (timestamp, message), tag)
    
    def start(self):
        """ Starts the UDP server. """
        self.lock()
        
        try:
            port = int(self.port_entry.get())
        except ValueError:
            self.write("Port not an integer, try again...", 'ERROR')
            self.stop()
            return
       
        self.port = reactor.listenUDP(port, self.server)

        self.write("Started the UDP server.")
    
    def stop(self):
        """ Stops the UDP server. """
        self.unlock()
        
        if self.port != None:
            self.port.stopListening()

        self.write("Stopped the UDP server.")

    def lock(self):
        """ Locks entry fields. """
        self.port_entry['state'] = DISABLED
        
        self.buttons['start']['state'] = DISABLED
        self.buttons['stop']['state']  = NORMAL
        
        self.status_string.set("started")

    def unlock(self):
        """ Unlocks entry fields. """
        self.port_entry['state'] = NORMAL
        
        self.buttons['start']['state'] = NORMAL
        self.buttons['stop']['state'] = DISABLED
        
        self.status_string.set("stopped")
