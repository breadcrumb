#-*- coding: utf8 -*-
"""Network code for the Breadcrumb server."""

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from twisted.internet import reactor
from twisted.internet.protocol import DatagramProtocol  # UDP
from twisted.internet.protocol import Protocol, Factory # TCP

import os
import sys
import time
import atexit
import logging
import decoding

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import common.consts as consts

class BreadcrumbServer:
    """A superclass for Breadcrumb servers."""
    def __init__(self, handler = None):
        self.handler = handler
        atexit.register(self.clean_up)

        self.keepdatapoints = False
        self.datapoints = []
    
    def new_crumb(self, packed):
        """Unpacks a new crumb, checks it, and passes it to the handler."""

        logging.debug('Got a new crumb, sending to decoder...')
        point = decoding.decode_crumb(packed)

        if self.keepdatapoints:
            self.datapoints.append(point)

        if 'handlerid' in point and 'handlermsg' in point:
            id, msg = point['handlerid'], point['handlermsg']
            self.handler.newmessage(id, msg)

        self.handler.newpoint(point)

    def clean_up(self):
        """Cleans up before closing.
        
        This tells the handler that we're about to shut down, so it has a
        chance to close connections, finalize files...
        """
        try:
            logging.info('Cleaning up (calling the clean_up method)...')
            self.handler.clean_up()
        except AttributeError:
            logging.error('... but the handler has no cleanup method!')
            logging.error('Does it inherit from BaseHandler properly?')
        except NotImplementedError:
            logging.debug("... but the handler didn't implement it.")

class BreadcrumbTCPServer(BreadcrumbServer, Protocol):
    """A TCP server for the breadcrumb protocol."""
    def __init__(self, handler = None):
        BreadcrumbServer.__init__(self, handler)
 
    def dataReceived(self, crumb):
        """Handles a new crumb, received over TCP."""
        self.new_crumb(crumb)

class BreadcrumbUDPServer(BreadcrumbServer, DatagramProtocol):
    """A UDP server for the breadcrumb protocol."""
    def __init__(self, handler = None):
        BreadcrumbServer.__init__(self, handler)
        logging.debug("New BreadcrumbUDPServer instance created.")
    
    def datagramReceived(self, crumb, (host, port)):
        """Handles a new crumb, received over UDP."""
        logging.debug("received something from %s:%d" % (host, port))
        self.new_crumb(crumb)

def start_server(port = None, handler = None, encap = 'UDP', **kwargs):
    """Runs the Breadcrumb server. 
    
    The server argument is either the server proper (for UDP servers) or the
    relevant factory (for TCP and derived protocols).
    """
    logging.info("Booting the Breadcrumb server (version %s)" % consts.VERSION)
    logging.debug("Server started at %s." % time.ctime())

    try:
        port = int(port)
    except ValueError:
        logging.error("Got noninteger port %s, can't start server..." % port)
        raise RuntimeError, 'Bad port: %s' % port

    encap = encap.upper()
    if encap == 'TCP':
        server = BreadcrumbTCPServer(handler = handler)
        
        factory = Factory()
        factory.protocol = server
        
        if port is None:
            port = consts.DEFAULT_TCP_PORT
        
        logging.debug("Prepared TCP reactor on port %d" % port)
        reactor.listenTCP(port, factory)
        
    elif encap == 'UDP':
        server = BreadcrumbUDPServer(handler = handler)
        
        if port is None:
            port = consts.DEFAULT_UDP_PORT
        
        logging.debug("Prepared UDP reactor on port %d" % port)
        reactor.listenUDP(port, server)
        
    else:
        raise RuntimeError, 'Unknown encapsulation %s' % encap
    
    reactor.run()
