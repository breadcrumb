# -*- coding: utf8 -*-
"""Functions for decoding crumbs."""

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import struct
import logging

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from common.checksum import generate_byte_checksum as generate_checksum
from common.consts import CRUMB_FIELDS

def decode_crumb(crumb):
    """Decodes a crumb into a dictionary."""
    # Unpack the crumb
    header, content, checksum = crumb[0], crumb[1:-1], crumb[-1]
    
    logging.debug("Got crumb: %s" % ':'.join('%02X' % ord(c) for c in crumb))

    # Checksum
    if generate_checksum(header + content) != checksum:
        logging.error("Got invalid checksum, ignoring this crumb...")
        return

    keys = []
    formats = ['<'] # forced little-endianness prevents useless padding

    fields = zip(unpack_bools(header), CRUMB_FIELDS)
    for present, field_data in fields:
        if present: # the field is contained in the crumb
            attr_name, attr_keys, attr_format = field_data
            logging.debug("Decoding attribute %s from crumb..." % attr_name)
            logging.debug("Keys: %s, format: %s" % (attr_keys, attr_format))

            keys.extend(attr_keys)
            formats.append(attr_format)

    logging.debug("Done decoding attributes from the header byte.")
    
    format = ''.join(formats)
    values = struct.unpack(format, content)
    
    point = dict(zip(keys, values))
    logging.debug("Point data: %s" % repr(point))
    
    return point

def unpack_bools(byte):
    """Unpacks a byte into a list of eight booleans.

    The byte is expected to be little-endian.
    
    >>> unpackbools('\\x00')
    [False, False, False, False, False, False, False, False]

    >>> unpackbools('\\x01')
    [True, False, False, False, False, False, False, False]

    >>> unpackbools('\\xff')
    [True, True, True, True, True, True, True, True]
    """
    return [bool((ord(byte) >> i) & 1) for i in xrange(8)]
