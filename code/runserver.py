#!/usr/bin/env python
# -*- coding: utf8 -*-
"""A script for starting a Breadcrumb server."""

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import logging

import optparse
import ConfigParser

import breadcrumb.server.net as net
import breadcrumb.server.handlers.base as base
import breadcrumb.common.consts as consts
from breadcrumb.common.loggingtools import get_logging_level

def main():
    """ Starts the default Breadcrumb server. """
    config = find_configuration()
    serverargs = get_serverargs(config)
    
    if 'verbosity' in serverargs:
        logger.setLevel(get_logging_level(serverargs['verbosity']))

    if 'pythonpath' in serverargs:
        sys.path.append(os.path.abspath(serverargs['pythonpath']))
        del serverargs['pythonpath']

    handler = parse_handlers(config)

    net.start_server(handler = handler, **serverargs)

def find_configuration(filename = None):
    """Tries to find and parse a server configuration file.

    Returns a ConfigParser object."""
    config = ConfigParser.ConfigParser()

    filenames = filter(None, consts.SERVER_CONFIG_FILENAMES + [filename])
    for filename in filenames:
        try:
            filename = os.path.expanduser(filename)
            config.readfp(open(filename))
            logger.info("Using config file %s..." % filename)
            return config
        except NameError:
            logger.debug("No config file at %s..." % filename)
            continue

    logger.warning("No usable config files found!")

def parse_handlers(config):
    """Parses the handlers described in the configuration file.

    Returns a sequential handler with all the (correctly configured) handlers
    from the configuration file.
    """
    seqhandler = base.SequentialHandler(name = "sequential")
    
    for name in config.sections():
        if name == "server":
            continue
        
        logger.debug("Found new handler section (name: %s)..." % name)

        data = dict(config.items(name) + [['name', name]])
        
        if 'class' not in data:
            logger.error(
                'No class specified for handler section %s, skipping...' % name)
            continue

        splitnames = data['class'].split('.')
        del data['class']
        modulename, classname = '.'.join(splitnames[:-1]), splitnames[-1]

        if 'pythonpath' in data:
            logger.debug("Extra path specified, adding to sys.path...")
            sys.path.append(data['pythonpath'])
            del data['pythonpath']

        try:
            __import__(modulename, globals(), locals(), [classname])
        except ImportError:
            logger.error("Couldn't import %s, skipping..." % modulename)
            continue

        handler_class = getattr(sys.modules[modulename], classname)

        logger.debug("Creating handler with args: %s" % repr(data))
        handler = handler_class(**data)
        
        seqhandler.add_handler(handler)

    return seqhandler

def get_serverargs(config):
    """Gets the server arguments from the ConfigParser object.
    
    Returns a dict.
    """
    if not config.has_section('server'):
        logger.info("Config file doesn't have a [server] section...")
        return {}

    return dict(config.items('server'))

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger("Breadcrumb.Scripts.runserver")
    main()
