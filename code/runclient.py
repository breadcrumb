#!/usr/bin/env python
# -*- coding: utf8 -*-
"""A script to start a typical breadcrumb session."""

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import optparse
import sys

import breadcrumb.client.gps as gps
import breadcrumb.client.tracking as tracking

def main(manual_args = None):
    """Tries to start a typical breadcrumb client.
    
    The optional manual_args arument should be a list that looks somewhat like
    sys.argv[1:] (a list of arguments). It is provided because WinCE doesn't 
    have system() -- this provides a workaround so you can start this function
    with different a different set of argvs using a small Python script.
    """
    opts = getopt(manual_args)
    set_verbosity(opts)

    logging.info('Breadcrumb client')
    
    tracker = tracking.Tracker(host = opts.host, port = opts.port)

    if opts.fake_gps:
        logging.debug("fake GPS specified...")
        gpsdevice = gps.connect(platform = 'fake',
                                sentence_filename = opts.fake_file)
    elif opts.gpsd_host and opts.gpsd_port:
        logging.debug("gpsd GPS specified...")
        gpsdevice = gps.connect(platform = 'gpsd',
                                host = opts.gpsd_host,
                                port = opts.gpsd_port)
    else:
        logging.debug("No GPS specified...")
        host, port = opts.gpsd_host, opts.serial_port
        gpsdevice = gps.connect(host = host, port = port)
    
    tracker.gps = gpsdevice
    
    tracker.start()
    tracker.gps.close()
    
    logging.debug("Closed the GPS port.")

def getopt(manual_args):
    """Gets the command line options."""
    optparser_config = {
        ('-F', '--fake-gps'): {
            'dest': 'fake_gps',
            'action': 'store_true',
            'help': 'Replay sentences with a simulated ("fake") GPS.'
        },

        ('', '--fake-file'): {
            'default': None,
            'dest': 'fake_file',
            'help': 'Uses a non-standard sentence file.',
        },

        ('-H', '--host'): {
            'dest': 'host',
            'help': 'The host to connect to.',
            'metavar': 'HOST',
        },

        ('-P', '--port'): {
            'default': None,
            'dest': 'port',
            'help': 'The TCP/UDP port the server is listening on.',
            'metavar': 'PORT',
        },
        
        ('-s', '--serial-port'): {
            'default': None,
            'dest': 'serial_port',
            'help': 'The serial port the GPS is on.',
            'metavar': 'PORT',
        },

        ('-g', '--gpsd-host'): {
            'default': None,
            'dest': 'gpsd_host',
            'help': 'The hostname of the gpsd server.',
            'metavar': "HOST",
        },
    
        ('-p', '--gpsd-port'): {
            'default': None,
            'dest': 'gpsd_port',
            'help': 'The port of the gpsd server.',
            'metavar': "PORT",
        },

        ('-v', '--verbose'): {
            'dest': 'verbose',
            'action': 'store_true',
            'help': 'Show debug messages.',
        },
    
        ('-q', '--quiet'): {
            'dest': 'quiet',
            'action': 'store_true',
            'help': 'Only show errors.',
        },

        ('-Q', '--very-quiet'): {
            'dest': 'veryquiet',
            'action': 'store_true',
            'help': "Don't even show errors (not recommended).",
        },
    }

    optparser = optparse.OptionParser()
    for flags, options in optparser_config.iteritems():
        optparser.add_option(*flags, **options)

    # We have no positional arguments:
    if manual_args is None:
        return optparser.parse_args()[0]
    else:
        return optparser.parse_args(manual_args)[0]

def set_verbosity(opts):
    """Sets the verbosity level of the logging module from optparser input."""
    if opts.verbose:
        logging.getLogger().setLevel(logging.DEBUG)
    elif opts.quiet:
        logging.getLogger().setLevel(logging.ERROR)
    elif opts.veryquiet:
        logging.getLogger().disable()
    else:
        logging.getLogger().setLevel(logging.INFO)

if __name__ == "__main__":
    main()
