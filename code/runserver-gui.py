#!/usr/bin/env python
# -*- coding: utf8 -*-
""" A GUI for running the Breadcrumb UDP server. """

# Copyright (C) 2008 Laurens Van Houtven <lvh at laurensvh.be>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from twisted.internet import tksupport, reactor
from time import strftime, ctime
from Tkinter import Tk

from server.handlers.base import SequentialHandler
from server.handlers.tkgui import TkGUIHandler
from server.net import BreadcrumbUDPServer

def main():
    """ Starts the GUI. """
    root = Tk()
    gui = TkGUIHandler(root)
    
    seqhandler = SequentialHandler() 
    seqhandler.addhandler(gui)
    
    server =  BreadcrumbUDPServer(handler = seqhandler)    
    
    gui.server = server
    
    tksupport.install(root)
    reactor.run()

if __name__ == "__main__":
    main()

